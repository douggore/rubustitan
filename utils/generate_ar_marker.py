import cv2
from cv2 import aruco

markerLength = 20   # Here, our measurement unit is centimetre.
markerDict = aruco.getPredefinedDictionary(aruco.DICT_4X4_50)
# markerDict = aruco.getPredefinedDictionary(aruco.DICT_6X6_50)

# calibrationFile = "calibrationFileName.xml"
# calibrationParams = cv2.FileStorage(calibrationFile, cv2.FILE_STORAGE_READ)
# camera_matrix = calibrationParams.getNode("cameraMatrix").mat()
# dist_coeffs = calibrationParams.getNode("distCoeffs").mat()

# Image processing function
def GenerateMarker(): #image):
    for i in range(0, 10):
        img = aruco.drawMarker(markerDict, i, 500)

        # print(image.shape)
        cv2.imshow("out", img)

        cv2.waitKey(0)

        cv2.imwrite("armarker_id{}.png".format(i), img)

    # while True:
    #     key = cv2.waitKey(1) & 0xFF

    #     if key == ord('q'):
    #         break

if __name__ == '__main__':
    print("Aruco marker generator")
    GenerateMarker()
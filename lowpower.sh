# Script to reduce the Pi's power consumption for a longer battery life

# Swtich off HDMI port
tvservice -o

# Switch off Ethernet / USB
sh -c 'echo 0x0 > /sys/devices/platform/soc/3f980000.usb/buspower'
# Rubus TITAN

Rubus TITAN is a competitor in the PiWars 4.0 competition to be held in Cambridge in April 2018. This is the source code for the robot which is designed to become an educational robotics platform after the competition.

## Supported hardware

- Raspberry Pi (all models)
- 4tronix Picon Zero
- Speaker pHAT
- Xbox One controller (optional)
- Official Raspberry Pi Camera (optional but recommended)

### Experimental/untested support (needs validation)

- Explorer pHAT / HAT

### Development platforms

- Windows

## Installation

Basic installation is straightforward, just enter the following:

    git clone https://codedin.wales/doug/rubustitan
    cd rubustitan
    pip3 install -r requirements.txt
    pip3 install .

## Usage

Running the project is easy. From your Raspberry Pi command line, run the following:

    rubustitan

Once running, the software will display a web address for you to connect to. By default, and for security purposes, the software will only initially allow connections from the web browser running on the Raspberry Pi by visiting http://localhost:8080

If you know what you're doing, and would like to expose the interface to all devices running on your network, then run the software using the following:

    python3 webbot.py -i 0.0.0.0

This will allow you to control the robot from any device on the same network as your Raspberry Pi at http://raspberrypi.lan:5000

### Control

Once connected in your browser, by default, you can use the following keys on your keyboard to control the robot:

- Arrow keys for forwards, backwards, left turn and right turn
- 'c' key to capture and display an image from the camera if connected
- 'u' key to turn on/off an ultrasonic distance measurement every 2 seconds

## License

This project has been developed by Douglas Gore and PiCymru as part of our education and learning programme. This project is hereby released under the terms of the MIT License, and is included below

    MIT License

    Copyright (c) 2018 Douglas Gore / PiCymru

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.

## Support

Have a question? Need assistance? Don't stay stuck! If you do use this project or have any feedback we would love to hear from you, you can add an issue to our [issue tracker](https://codedin.wales/doug/rubustitan), tweet us at [@PiCymru](https://twitter.com/PiCymru) or drop us an [e-mail](mailto:hello@picymru.org.uk)

## Acknowledgements

Coming soon
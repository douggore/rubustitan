# Controller for Pimoroni Explorer HAT

# from gpiozero import DistanceSensor
import explorerhat

class ExplorerHatController:
    def __init__(self, command_queue):
        explorerhat.motor.one.invert()
        explorerhat.motor.two.invert()

        self.queue = command_queue

        self.queue.register_commands(
            {
                "motor_speed": self.set_motor_speeds,
                # "read_distance": self.read_distance
            })

    def __del__(self):
        pass

    def set_motor_speed(self, motor, speed):
        # forwards = speed > 0.0
        mcu_speed = int(speed * 100)

        # Avoid stalls and thus power surges by preventing really low PWM
        # if (mcu_speed > 0) and (mcu_speed < 15):
        #     mcu_speed = 15

        print("Motor = {}, speed = {}".format(motor, mcu_speed))

        if motor == 1:
            explorerhat.motor.one.speed(mcu_speed)
        else:
            explorerhat.motor.two.speed(mcu_speed)

    def set_motor_speeds(self, data):
        for key, value in data.items():
            if key == "motor0":
                self.set_motor_speed(0, value)
            elif key == "motor1":
                self.set_motor_speed(1, value)

    # def read_distance(self, data):
    #     self.queue.send("read_distance_response", {
    #             "distance": distance_sensor.distance()
    #         })

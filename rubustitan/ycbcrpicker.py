import cv2
import numpy as np
import json
import argparse

parser = argparse.ArgumentParser()

parser.add_argument("input_file", help="Colour data file to load", nargs="?", default="colour_data.json")

args = parser.parse_args()

cap = cv2.VideoCapture(0)

lY, lCb, lCr = 0, 0, 0

# Starting with 100's to prevent error while masking
hY, hCb, hCr = 255, 255, 255


def loadColour():
    print("Loading colour data file '{}'".format(args.input_file))
    with open(args.input_file) as f:
        data = json.load(f)

    return data

def saveColour():
    with open("colour_data.json", "w") as f:
        # pickle.dump([(lY, lCb, lCr), (hY, hCb, hCr)], f)

        colour_dat = {
                "low": (lY, lCb, lCr),
                "high": (hY, hCb, hCr)
        }

        json.dump(colour_dat, f)
        # json.dump([(lY, lCb, lCr), (hY, hCb, hCr)], f)

def nothing(x):
    pass

# Creating a window for later use
cv2.namedWindow('result')

data_file = "colour_data.json"

try:
    data = loadColour()

    lY, lCb, lCr = data["low"]
    hY, hCb, hCr = data["high"]
except:
    print("ERROR: No colour data loaded")


# Creating track bar
cv2.createTrackbar('L Y', 'result', lY, 255, nothing)
cv2.createTrackbar('L Cb', 'result', lCb, 255, nothing)
cv2.createTrackbar('L Cr', 'result', lCr, 255, nothing)

cv2.createTrackbar('H Y', 'result', hY, 255, nothing)
cv2.createTrackbar('H Cb', 'result', hCb, 255, nothing)
cv2.createTrackbar('H Cr', 'result', hCr, 255, nothing)

while(1):

    _, frame = cap.read()

    #converting to HSV
    ycbcr = cv2.cvtColor(frame, cv2.COLOR_BGR2YCrCb)

    # get info from track bar and appy to result
    lY = cv2.getTrackbarPos('L Y','result')
    lCb = cv2.getTrackbarPos('L Cb','result')
    lCr = cv2.getTrackbarPos('L Cr','result')

    hY = cv2.getTrackbarPos('H Y','result')
    hCb = cv2.getTrackbarPos('H Cb','result')
    hCr = cv2.getTrackbarPos('H Cr','result')

    # Normal masking algorithm
    lower_range = np.array([lY, lCb, lCr])
    # upper_blue = np.array([180,255,255])
    upper_range = np.array([hY, hCb, hCr])

    mask = cv2.inRange(ycbcr, lower_range, upper_range)

    result = cv2.bitwise_and(frame,frame,mask = mask)

    cv2.imshow('vision',result)

    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break

saveColour()

cap.release()

cv2.destroyAllWindows()
# manualdrive.py

from rubustitan import MotorMixer

class ManualDrive:
    # @property
    # def mode(self):
    #     return "manual"

    def __init__(self, command_queue):
        self.queue = command_queue
        self.yaw = 0.0
        self.throttle = 0.0
        self.headlights = False

    def enter(self):
        print("Enter ManualDrive")
        self.queue.register_commands(
            {
                "operator_input": self.control_data,
                "button_presses": self.button_pressed
            })

    def exit(self):
        print("Exit ManualDrive")
        self.queue.unregister_commands(["operator_input", "button_presses"])

    def control_data(self, data):
        for key, value in data.items():
            if key == "yaw":
                self.yaw = value
            elif key == "throttle":
                self.throttle = value

        power_left, power_right = MotorMixer.combined_scaled_turn(self.yaw, self.throttle)

        if self.yaw == 0 and self.throttle == 0.0:
            print("Motors stopped")
        
        # print("Motor power (L, R): ", power_left, power_right)

        self.queue.send("motor_speed",
                        {
                            "motor0": power_right,
                            "motor1": power_left
                        })

    def button_pressed(self, data):
        if "l1" in data:
            print("Toggle headlights")
            self.headlights = not self.headlights
            self.queue.send("leds",
            {
                "zone": "front",
                "state": "on" if self.headlights else "off"
            })

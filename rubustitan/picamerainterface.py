import picamera
import picamera.array
from time import sleep
from threading import Thread, Event

class PiCameraInterface:
    def __init__(self):
        self.camera = None
        self.stream = None
        self.callback = None
        self.processing_thread = None
        self.capture_thread = None
        self.event = Event()
        # self.terminated = False
        self.running = False
        
    def start_capture(self, callback, resolution=(320, 240), framerate=8):
        self.callback = callback
        self.running = True
        self.camera = picamera.PiCamera()

        # Enable if the camera is mounted upside down
        # self.camera.hflip = True
        # self.camera.vflip = True
        
        self.camera.resolution = resolution
        self.camera.framerate = framerate
        self.camera.exposure_mode = "sports"
        self.stream = picamera.array.PiRGBArray(self.camera)
        self.processing_thread = Thread(target=self.processing_main)
        self.processing_thread.start()

        self.capture_thread = Thread(target=self.capture_main)
        self.capture_thread.start()

    def capture_main(self):
        self.camera.capture_sequence(self.TriggerStream(), format='bgr', use_video_port=True)

    def processing_main(self):
        # This method runs in a separate thread
        # while not self.terminated:
        while self.running:
            # Wait for an image to be written to the stream
            if self.event.wait(1):
                try:
                    # Read the image and do some processing on it
                    self.stream.seek(0)
                    self.callback(self.stream.array)
                finally:
                    # Reset the stream and event
                    self.stream.seek(0)
                    self.stream.truncate()
                    self.event.clear()

    # Stream delegation loop
    def TriggerStream(self):
        while self.running:
            if self.event.is_set():
                sleep(0.01)
            else:
                yield self.stream
                self.event.set()


    def stop_capture(self):
        self.running = False
        print("Stop camera capture, join threads")
        self.processing_thread.join()
        self.capture_thread.join()
        print("Close camera")
        self.camera.close()
        # self.capture.release()
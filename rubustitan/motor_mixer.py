from math import copysign

class MotorMixer:
    @staticmethod
    def swing_turn(yaw, throttle):
        """
        Mix a pair of joystick axes, returning a pair of wheel speeds. This is where the mapping from
        joystick positions to wheel powers is defined, so any changes to how the robot drives should
        be made here, everything else is really just plumbing.
        
        :param yaw:
            Yaw axis value, ranges from -1.0 to 1.0
        :param throttle:
            Throttle axis value, ranges from -1.0 to 1.0
        :return:
            A pair of power_left, power_right integer values to send to the motor driver
        """

        clamp = lambda n, minn, maxn: max(min(maxn, n), minn)

        left = clamp(throttle + yaw, -1.0, 1.0)
        right = clamp(throttle - yaw, -1.0, 1.0)

        return (left * abs(throttle)), (right * abs(throttle))

    @staticmethod
    def point_turn(yaw):
        """
        Turn left or right on the spot depending on the magnitude of the yaw.
        
        :param yaw:
            Yaw axis value, ranges from -1.0 to 1.0
        :return:
            A pair of power_left, power_right integer values to send to the motor driver
        """

        clamp = lambda n, minn, maxn: max(min(maxn, n), minn)

        # left = clamp(throttle + yaw, -1.0, 1.0)
        # right = clamp(throttle - yaw, -1.0, 1.0)

        # return (left * abs(throttle)), (right * abs(throttle))

        return (yaw, -yaw)

    @classmethod
    def combined_turn(cls, yaw, throttle):
        if throttle == 0.0:
            return cls.point_turn(yaw)

        return cls.swing_turn(yaw, throttle)

    @classmethod
    def combined_scaled_turn(cls, yaw, throttle):
        if throttle == 0.0:
            if yaw == 0.0:
                return (0.0, 0.0)
            else:
                return cls.point_turn(copysign(0.5, yaw) + (yaw * 0.3))

        return cls.swing_turn(yaw, throttle)
function updateBatteryState() {
    fetch("/camera/battery").then(function(response) {
        return response.text();
      }).then(function(text) {
        // var objectURL = URL.createObjectURL(myBlob);
        // myImage.src = objectURL;
        console.log("Battery state = " + text);
        let batteryLevel = parseFloat(text);
        if (batteryLevel < 10) {
            $("#batteryLevel").attr("src", "images/battery-0.png");
        } else if (batteryLevel < 33) {
            $("#batteryLevel").attr("src", "images/battery-33.png");
        } else if (batteryLevel < 66) {
            $("#batteryLevel").attr("src", "images/battery-66.png");
        } else {
            $("#batteryLevel").attr("src", "images/battery-100.png");
        }
      });
}

// Convert sounds list to visual buttons
function displaySoundsList(soundsList) {
	var soundsListNav = document.getElementById("soundsList");
	soundsListNav.innerHTML = "";

	for (let sound of soundsList) {
		let element = document.createElement("button");
		element.className = "blueButton buttonListItem";

		element.innerText = sound;
		element.addEventListener("click", function() { playSound(sound) }, false);

		soundsListNav.appendChild(element);
	}
}

function playSound(soundFile) {
	sendCommand("play_sound",
		{
			"sound": soundFile
		});
}

function updateStatus() {
    console.log("Update status");
    sendCommand("battery", {});
}

function updateBatteryImage(elementId, batteryLevel) {
    let element = document.getElementById(elementId);

    if (batteryLevel < 10) {
        element.src = "images/battery-0.png";
    } else if (batteryLevel < 33) {
        element.src = "images/battery-33.png";
    } else if (batteryLevel < 66) {
        element.src = "images/battery-66.png";
    } else {
        element.src = "images/battery-100.png";
    }
}

function updateBattery(data) {
    console.log("Battery 0 = " + data.battery0);
    console.log("Battery 1 = " + data.battery1);

    // updateBatteryImage("lionBattery", data.battery0);
    document.getElementById("lionLevel").innerHTML = `${data.battery0}%`;
    document.getElementById("nimhLevel").innerHTML = `${data.battery1}%`;
}

function nextMode() {
	sendCommand("cycle_mode", {});    
}

function start() {
	sendCommand("button_presses", ["start"]);    
}

function squareButton() {
	sendCommand("button_presses", ["square"]);    
}

function crossButton() {
	sendCommand("button_presses", ["cross"]);    
}

function ultrasonicResponse(distance) {
	let topwall = document.getElementById("topwall");
	let decDistance = Math.round(distance);

	console.log(`Ultrasonic distance = ${distance} cm`);

	let offset = 45 - (distance / 10);
	
	topwall.setAttribute("transform", `translate(0, ${offset})`);
	topwall.children[1].innerHTML = `${decDistance} cm`;
}

function headingResponse(heading) {
	console.log(`Heading = ${heading} degrees`);

	let headingAni = document.getElementById("headingAni");
	let oldHeading = headingAni.getAttribute("to", heading);

	let label = document.getElementById("deglabel");

	label.textContent = heading.toFixed(1) + "\u00b0";

	headingAni.setAttribute("from", oldHeading);
	headingAni.setAttribute("to", heading);
	headingAni.beginElement();
}

function updateSensors() {
	sendCommand("read_distance", {})
	sendCommand("read_heading", {})
}

function registerCommand(command) {
	var registerStructure = {
		register: {
			"command": command
		}
	};

	websock.send(JSON.stringify(registerStructure));
}

function sendCommand(command, data) {
	var sendStructure = {
		send: {
			"command" : command,
			"data": data
		}
	};

	websock.send(JSON.stringify(sendStructure));
}

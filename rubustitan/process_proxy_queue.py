# ProcessProxyQueue
from multiprocessing import Queue
from threading import Thread

class ProcessProxyQueue:
    def __init__(self, shared_queue_local, shared_queue_remote, local_queue=None):
        self.command_map = {}
        self.local_queue = local_queue
        self.shared_queue_local = shared_queue_local
        self.shared_queue_remote = shared_queue_remote
        self.thread = Thread(target=self.relay_main)

    def start(self):
        self.thread.start()

    def stop(self):
        if self.thread.is_alive():
            item = {
                "type": "exit",
                "command": "exit"
            }
            self.shared_queue_local.put(item)

            self.thread.join()

    def send(self, command, obj):
        item = {
            "type": "send",
            "command": command,
            "obj": obj
        }
        self.shared_queue_remote.put(item)

    def register_commands(self, cmds):
        """Register one or more commands and their callback functions in the system.

        Args:
            cmds (dict): Dictionary of commands and callbacks
        """
        # for command, function in cmds.items():
        #     self.command_map.setdefault(command, []).append(
        #         {
        #             "callback": function,
        #             "owner": self.get_calling_class()
        #         })
        for command, function in cmds.items():
            self.command_map[command] = function

            item = {
                "type": "register",
                "command": command
            }
            self.shared_queue_remote.put(item)

    def unregister_commands(self, cmds):
        for command in cmds:
            if command in self.command_map:
                item = {
                    "type": "unregister",
                    "command": command
                }
                self.shared_queue_remote.put(item)

                del self.command_map[command]

    def relay_command(self, command, data):
        item = {
            "type": "recv",
            "command": command,
            "obj": data
        }
        self.shared_queue_remote.put(item)

    def proxy_callback_generator(self, key):
        def callback(value):
            self.relay_command(key, value)
        return callback

    def relay_main(self):
        running = True

        while running:
            item = self.shared_queue_local.get()

            if item["command"] == "exit":
                running = False
            elif self.local_queue:
                if item["type"] == "send":
                    self.local_queue.send(item["command"], item["obj"])
                elif item["type"] == "register":
                    self.local_queue.register_commands({
                        item["command"]: self.proxy_callback_generator(item["command"])
                    })
                elif item["type"] == "unregister":
                    self.local_queue.unregister_commands([ item["command"] ])
                else:
                    print("ERROR: Unable to handle this local relay event")
            else:
                if item["type"] == "recv":
                    if item["command"] in self.command_map:
                        self.command_map[item["command"]](item["obj"])
                    else:
                        print("ERROR: Command '{}' not registered".format(item["command"]))
                else:
                    print("ERROR: Unable to handle this remote relay event")

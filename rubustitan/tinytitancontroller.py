# Controller for Pimoroni Explorer HAT

import explorerhat
import time

from gpiozero import DistanceSensor
from rubustitan.lsm6ds3 import IMU
from threading import Thread
from time import sleep
from simple_pid import PID

class TinyTitanController:
    def __init__(self, command_queue):
        explorerhat.motor.one.invert()
        explorerhat.motor.two.invert()

        self.queue = command_queue

        self.queue.register_commands(
            {
                "motor_speed": self.set_motor_speeds,
                "read_heading": self.read_heading,
                "read_distance": self.read_distance
            })

        self.imu = IMU()
        self.pid = PID(1, 0.1, 0.05, setpoint=0)
        self.heading = 0
        self.motor_speed = 0
        self.left_motor_scale = 1.0
        self.right_motor_scale = 0.8

        self.pid_enable = False
        self.front_distance_sensor = DistanceSensor(echo=18, trigger=17, max_distance=4, queue_len=3)

        self.thread = Thread(target=self.thread_main)
        self.thread.start()

    def __del__(self):
        pass

    def set_motor_speed(self, motor, speed):
        # pass
        mcu_speed = int(speed * 100)

        # Avoid stalls and thus power surges by preventing really low PWM
        if (mcu_speed > 0) and (mcu_speed < 15):
            mcu_speed = 15

        # print("Motor = {}, speed = {}".format(motor, mcu_speed))

        if motor == 1:
            explorerhat.motor.one.speed(mcu_speed)
        else:
            explorerhat.motor.two.speed(mcu_speed)

    def set_motor_speeds(self, data):
        self.pid_enable = False

        for key, value in data.items():
            if key == "motor0":
                self.motor_speed = value
                self.set_motor_speed(0, value * self.left_motor_scale)
                # self.set_motor_speed(0, value)
            elif key == "motor1":
                self.set_motor_speed(1, value * self.right_motor_scale)
                # self.set_motor_speed(1, value)
            elif key == "pid":
                self.pid_enable = True
                self.pid.setpoint = self.heading

    def read_distance(self, data):
        self.queue.send("read_distance_response", {
            "front": self.front_distance_sensor.distance * 100
        })

    def read_heading(self, data):
        self.queue.send("read_heading_response", {
            "heading": self.heading
        })

    def thread_main(self):
        sleep_period = self.imu.gyro_sleep_period()
        # self.pid.sample_time = sleep_period

        print("Calibrating gyro")
        self.imu.calibrate_gyro()

        prev_time = time.time()
        gyro_z_angle = 0

        while True:
            gyro_z = self.imu.gyroZ()

            #Calculate loop Period(LP). How long between Gyro Reads
            loop_period = time.time() - prev_time
            prev_time = time.time()

            gyro_z_angle += gyro_z * loop_period
            self.heading = gyro_z_angle

            # print("Gyro Z = {}, angle = {}, LP = {:.2f}".format(gyro_z, gyro_z_angle, loop_period))
            sleep(sleep_period)

            if self.pid_enable is True:
                # pv = self.pid(self.heading)
                # print("Gyro Z = {:.2f}, PID output = {:.2f}, SP = {:.2f}".format(gyro_z_angle, pv, self.pid.setpoint))
                self.adjust_motors(gyro_z_angle)

    def adjust_motors(self, pid_value):
        if pid_value < 0:
            adjustment = 1.0 - (abs(max(pid_value, -90)) / 90)
            self.left_motor_scale = adjustment
            self.right_motor_scale = 1.0
        else:
            adjustment = 1.0 - (min(pid_value, 90) / 90)
            self.left_motor_scale = 1.0
            self.right_motor_scale = adjustment

        print("Angle {:.2f} deg, motor scale L: {:.3f}, R: {:.3f}".format(pid_value, self.left_motor_scale, self.right_motor_scale))

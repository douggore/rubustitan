# Controller for the custom Rubus TITAN robotics board

import time

from rubustitan import rubuslib
from gpiozero import DistanceSensor
from time import sleep
from threading import Thread
from rubustitan.lsm6ds3 import IMU

# Big One Nine wheels
motor_scales = [ 1.0, 0.80 ]

# Standard wheels (with masking tape)
motor_scales = [ 1.0, 0.90 ]

# motor_scales = [ 1.0, 0.91 ]
# motor_scales = [ 1.0, 1.0 ]

class RubusController:
    def __init__(self, command_queue):
        self.queue = command_queue
        self.imu = IMU()
        self.heading = 0

        self.front_distance_sensor = DistanceSensor(echo=23, trigger=27)
        # self.rear_distance_sensor = DistanceSensor(echo=24, trigger=22)

        self.queue.register_commands(
            {
                "motor_speed": self.set_motor_speeds,
                "read_distance": self.read_distance,
                "read_heading": self.read_heading,
                "battery": self.read_battery,
                "leds": self.set_leds,
                "projectile": self.launch_projectile
            })

        self.thread = Thread(target=self.thread_main)
        self.thread.start()

    def __del__(self):
        self.unregister()

    def unregister(self):
        self.queue.unregister_commands(["motor_speed", "read_distance", "read_heading", "battery", "leds", "projectile"])

    def set_motor_speed(self, motor, speed):
        forwards = speed > 0.0
        mcu_speed = int(abs(speed * motor_scales[motor]) * 127)

        # Avoid stalls and thus power surges by preventing really low PWM
        if (mcu_speed > 0) and (mcu_speed < 15):
            mcu_speed = 15

        # print("Input speed = {}".format(speed))
        # print("Motor = {}, Dir = {}, speed = {}".format(motor, forwards, mcu_speed))

        if motor == 1:
            rubuslib.set_left_motor(forwards, mcu_speed)
        else:
            rubuslib.set_right_motor(forwards, mcu_speed)

    def set_motor_speeds(self, data):
        for key, value in data.items():
            if key == "motor0":
                self.set_motor_speed(0, value)
            elif key == "motor1":
                self.set_motor_speed(1, value)

    def read_distance(self, data):
        print("Test, distance = {} cm".format(self.front_distance_sensor.distance * 100))
        self.queue.send("read_distance_response", {
                "front": self.front_distance_sensor.distance * 100
                # "rear": self.rear_distance_sensor.distance * 100
            })

    def read_heading(self, data):
        self.queue.send("read_heading_response", {
            "heading": self.heading
        })

    def read_battery(self, data):
        pass
        lion, nimh = rubuslib.battery_voltages()
        lion_percentage = rubuslib.battery_voltage_as_percent(lion)
        nimh_percentage = rubuslib.battery_voltage_as_percent(nimh)

        self.queue.send("battery_response", {
            "battery0": rubuslib.battery_voltage_as_percent(lion_percentage),
            "battery1": rubuslib.battery_voltage_as_percent(nimh_percentage)
        })

    def set_leds(self, data):
        if "zone" in data and data["zone"] == "front":
            if data["state"] == "on":
                print("Set Rubus LEDs on")
                rubuslib.enable_headlights()
            else:
                print("Set Rubus LEDs off")
                rubuslib.disable_headlights()

    def launch_projectile(self, data):
        rubuslib.set_servo_rotation(45)
        rubuslib.enable_relay()
        sleep(2)
        rubuslib.enable_servo()
        sleep(1)
        # rubuslib.set_servo_rotation(80)
        rubuslib.set_servo_rotation(120)
        sleep(1)
        rubuslib.disable_servo()
        rubuslib.disable_relay()

        self.queue.send("projectile_launched", {})

    def thread_main(self):
        sleep_period = self.imu.gyro_sleep_period()

        print("Detecting...")
        if not self.imu.detected():
            print("IMU not detected")
            return

        print("Calibrating gyro")
        self.imu.calibrate_gyro()

        prev_time = time.time()
        gyro_z_angle = 0

        while True:
            gyro_z = self.imu.gyroZ()

            #Calculate loop Period(LP). How long between Gyro Reads
            loop_period = time.time() - prev_time
            prev_time = time.time()

            gyro_z_angle += gyro_z * loop_period

            if gyro_z_angle > 360.0:
                gyro_z_angle -= 360.0
            elif gyro_z_angle < 0.0:
                gyro_z_angle += 360.0

            self.heading = gyro_z_angle

            # print("Gyro Z = {}, angle = {}, LP = {:.2f}".format(gyro_z, gyro_z_angle, loop_period))
            sleep(sleep_period)

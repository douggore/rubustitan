# logicmanager.py

# import importlib.util

from collections import OrderedDict
import itertools

from rubustitan import ManualDrive, LineFollower, MarsMaze, Rainbow, DuckShoot

# 2017 competition mode
# MinimalMaze

class LogicManager:
    def __init__(self, command_queue, initial_module):
        self.queue = command_queue
        self.logic_class = None

        self.logic_modes = OrderedDict({
            # Manual modes
            "manualdrive": ManualDrive(command_queue), # Obstactle course, Pi Noon?, golf
            "spaceinvaders": DuckShoot(command_queue),
            # Autonomous modes
            "blastoff": LineFollower(command_queue),
            "marsmaze": MarsMaze(command_queue),
            "nebula": Rainbow(command_queue)
        })

        self.logic_iter = itertools.cycle(self.logic_modes)

        self.queue.register_commands(
            {
                "cycle_mode": self.cycle_mode
            })

    def __del__(self):
        self.cleanup()

    def start(self):
        initial_mode = next(self.logic_iter)
        self.logic_class = self.logic_modes[initial_mode]

        self.queue.send("mode_changed", initial_mode)
        
        self.logic_class.enter()

    def cleanup(self):
        self.queue.unregister_commands(["cycle_mode"])
        self.logic_class.exit()

    def cycle_mode(self, data):
        self.logic_class.exit()

        next_mode = next(self.logic_iter)
        self.logic_class = self.logic_modes[next_mode]
        self.queue.send("mode_changed", next_mode)
        
        self.logic_class.enter()

from .motor_mixer import MotorMixer
from .cmdqueue import CommandQueue
from .process_proxy_queue import ProcessProxyQueue
from .webcontroller import WebController
from .soundcontroller import SoundController
# from .ballprocessor import BallProcessor
# from .ar_marker import ArMarkerProcessor
from .colourprocessor import ColourProcessor
from .lineprocessor import LineProcessor
from .edgeprocessor import EdgeProcessor
from .visioncontroller import VisionController
from .manualdrive import ManualDrive
from .duckshoot import DuckShoot
from .linefollower import LineFollower
# from .minimalmaze import MinimalMaze
from .ultrasonic_maze import MarsMaze
from .rainbow import Rainbow
from .logicmanager import LogicManager
from gevent import monkey
monkey.patch_all()

import os
import sys
import json

from bottle import Bottle, route, request, response, redirect, hook, error, default_app, view, static_file, template, HTTPError, abort

from bottle.ext.websocket import GeventWebSocketServer
from bottle.ext.websocket import websocket
from geventwebsocket import WebSocketError

from threading import Thread

# from gevent.pywsgi import WSGIServer
from geventwebsocket.handler import WebSocketHandler

__import__('http.server').server.BaseHTTPRequestHandler.address_string = lambda x:x.client_address[0]

def methodroute(route):
    def decorator(f):
        f.route = route
        return f
    return decorator

class WebController(Bottle):
    def __init__(self, command_queue):
        super(WebController, self).__init__()
        self.server = None
        self.thread = None
        self.queue = command_queue
        self.route('/', callback=self.index)
        self.route('/<filepath:path>', callback=self.index2)
        self.route('/websocket', callback=self.handle_websocket)

    # @methodroute('/')
    def index(self):
        return static_file('index.html', root='public')

    # @methodroute('/<filepath:path>')
    def index2(self, filepath):
        script_path = os.path.dirname(__file__)
        return static_file(filepath, root=os.path.join(script_path, 'public'))

    # @methodroute('/websocket')
    def handle_websocket(self):
        command_list = []

        self.wsock = request.environ.get('wsgi.websocket')
        if not self.wsock:
            abort(400, 'Expected WebSocket request.')

        while True:
            try:
                json_data = self.wsock.receive()

                # Check there is some data to process
                if json_data:
                    # Convert JSON object to Python object
                    data = json.loads(json_data)
                    
                    for action, payload in data.items():
                        if action == "register":
                            command = payload["command"]
                            command_list.append(command)
                            self.queue.register_commands({ command: self.proxy_callback_generator(command) })
                        elif action == "send":
                            # Send commands on the queue
                            self.queue.send(payload["command"], payload["data"])
                    
                    # wsock.send("Your message was: %r" % data)
            except WebSocketError:
                break

        self.queue.unregister_commands(command_list)

    def proxy_callback_generator(self, key):
        def callback(value):
            self.command_proxy(key, value)
        return callback

    def command_proxy(self, key, data):
        send_object = { key: data }
        encoded_object = json.dumps(send_object)
        self.wsock.send(encoded_object)

    def start(self):
        # self.app = default_app()
        # app.run(host="0.0.0.0", port="8080", server='paste')

        print("Create webserver")
        print("Create thread")
        self.thread = Thread(target=self.run, kwargs=dict(host='0.0.0.0', port=8080, fast=True, server=GeventWebSocketServer))
        self.thread.daemon = True
        print("Start thread")
        self.thread.start()
        print("Thread started")

    def stop(self):
        print("Shutdown webserver")
        # self.shutdown()
        print("Stop")
        # super(WebController, self).stop()
        print("Close")
        # self.close()
        print("Close")
        # sys.stderr.close()
        print("Terminate thread")
        # self.thread.join()
        # gevent.joinall([self.greenlet])

import cv2
from cv2 import aruco

class ArMarkerProcessor:
    # PreferredResolution = (640, 480)
    PreferredResolution = (320, 240)

    def __init__(self, queue):
        self.queue = queue
        self.debug = False
        self.marker_dict = aruco.getPredefinedDictionary(aruco.DICT_4X4_50)

        self.queue.register_commands(
            {
                "visual_debug": self.toggle_debug
            })

    def __del__(self):
        self.queue.unregister_commands(["visual_debug"])

    def toggle_debug(self, _):
        self.debug = not self.debug

        if not self.debug:
            cv2.destroyAllWindows()

    def process_image(self, image):
        height, width, _ = image.shape

        # Convert to greyscale (we can do this in HW on the Pi)
        image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) 
        
        # Get coordinates & ids, ignore rejects
        corners, ids, _ = aruco.detectMarkers(image_gray, self.marker_dict)

        # If at least one marker detected
        if ids is not None:
            if self.debug:
                # Draw markers
                imgWithAruco = aruco.drawDetectedMarkers(image, corners, ids, (0,255,0))

            # Extract the left and right edges of the marker
            top_left, top_right, bottom_right, _ = corners[0][0]

            # Calculate the offset within the image for the very centre of the target
            targetCentreX = top_left[0] + ((top_right[0] - top_left[0]) / 2)

            # print("Target centre = {}, image centre = {}".format(targetCentreX, centreX))

            # Calculate a value between -1.0 and 1.0 to represent the marker relative to
            # the centre of the camera's vision
            target_offset = ((targetCentreX / width) - 0.5) * 2.0
            
            target_width = top_right[0] - top_left[0]
            target_height = bottom_right[1] - top_right[1]
            area = (target_width * target_height) / (width * height)

            # print("ID = {}, centre offset = {}".format(ids[0], target_offset))

            self.queue.send("armarker", {
                "id": ids[0],
                "hoffset": target_offset,
                "height": target_height,
                "area": area
            })
        elif self.debug:
            imgWithAruco = image

        if self.debug:
            cv2.imshow("out", imgWithAruco)
            cv2.waitKey(1)

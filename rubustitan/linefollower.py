# manualdrive.py
from time import sleep
from threading import Thread, Event
from rubustitan import MotorMixer

class LineFollower:
    def __init__(self, command_queue):
        self.queue = command_queue

        self.thread = None
        self.running = False

        self.activate = False
        self.line_notified = Event()
        self.offset = 0.0

        self.headlights = False

    def enter(self):
        print("Enter LineFollower")
        self.queue.register_commands(
            {
                "button_presses": self.buttons_pressed,
                "line_found": self.line_found
            })

        self.thread = Thread(target=self.thread_main)
        self.thread.start()

        self.queue.send("start_camera", {
            "processor": "line"
        })

    def exit(self):
        print("Exit LineFollower")
        self.activate = False
        self.running = False
        self.thread.join()

        self.queue.unregister_commands(["line_found", "button_presses"])
        self.queue.send("stop_camera", {})        

    def thread_main(self):
        self.running = True

        while self.running:
            if self.activate:
                # self.queue.send("play_sound", { "sound": "activate" })

                while self.activate:
                    print("Wait for line")
                    self.line_notified.wait(0.5)

                    if self.line_notified.is_set():
                        self.line_notified.clear()

                        print("Position ({:.2f}) apply correction factor {:.2f}".format(self.offset, -self.offset))

                        drive_left, drive_right = MotorMixer.swing_turn(self.offset, 0.7)

                        self.queue.send("motor_speed",
                                        {
                                            "motor0": drive_left,
                                            "motor1": drive_right
                                        })

                    else:
                        self.queue.send("motor_speed",
                                        {
                                            "motor0": 1.0,
                                            "motor1": 1.0
                                        })

                self.activate = False

                self.queue.send("motor_speed",
                                {
                                    "motor0": 0.0,
                                    "motor1": 0.0
                                })
            else:
                sleep(1)

        print("Line follower thread terminating")

    def buttons_pressed(self, data):
        if "start" in data:
            self.activate = not self.activate
            print("Autonomous activate = ", self.activate)
        elif "circle" in data:
            self.queue.send("visual_debug", {})
        elif "l1" in data:
            print("Toggle headlights")
            self.headlights = not self.headlights
            self.queue.send("leds",
            {
                "zone": "front",
                "state": "on" if self.headlights else "off"
            })

    def line_found(self, data):
        self.offset = data["hoffset"]
        self.line_notified.set()
#!/usr/bin/env python3

##
# webbot - A browser controlled robot! Your little frIEnd.
##
# Copyright (c) 2016 PiCymru
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os, logging, subprocess, time, argparse
from bottle import route, request, response, redirect, hook, error, default_app, view, static_file, template, HTTPError
from bluedot import BlueDot

from piconcontroller import PiconController

def bluedot_move(position):
    robot_speed = int(127 * position.distance)
    print("Distance = {}, robot speed = {}".format(position.distance, robot_speed))
    if position.top:
        print("Blue forwards")
        controller.move_forwards(position.distance)
    elif position.bottom:
        print("Blue back")
        controller.move_backwards(position.distance)
    elif position.left:
        print("Blue left")
        controller.move_left(position.distance)
    elif position.right:
        print("Blue right")
        controller.move_right(position.distance)

def bluedot_release():
    print("Blue stop")
    controller.move_stop()

@route('/left')
def action_left():
    controller.move_left(1.0)
    time.sleep(0.1)
    controller.move_stop()
    return "LEFT TURN"

@route('/right')
def action_right():
    controller.move_right(1.0)
    time.sleep(0.1)
    controller.move_stop()
    return "RIGHT TURN"

@route('/forward')
@route('/forwards')
def action_forward():
    controller.move_forwards(1.0)
    time.sleep(0.2)
    controller.move_stop()
    return "FORWARDS"

@route('/back')
@route('/backward')
def action_back():
    controller.move_backwards(1.0)
    time.sleep(0.2)
    controller.move_stop()
    return "BACKWARDS"

@route('/ultrasonic')
def ultrasonic():
    return "{:.2f}".format(controller.read_distance())

@route('/cheese')
def cheese():
    response.content_type = 'image/jpeg'
    response.cache_control = 'no-store'
    with subprocess.Popen(["raspistill", "-vf", "-w", "400", "-h", "300", "-o", "-"], stdout=subprocess.PIPE) as proc:
        return proc.stdout.read()
    return run_output.stdout

@route('/')
def index():
    return static_file('index.html', root='public')

@route('/style.css')
def index():
    return static_file('style.css', root='public')

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    # Server settings
    parser.add_argument("-i", "--host", default=os.getenv('IP', '127.0.0.1'), help="IP Address")
    parser.add_argument("-p", "--port", default=os.getenv('PORT', 5000), help="Port")

    # Additional hardware
    parser.add_argument("--line-sensor", help="enable line sensor", default=False, action="store_true")

    # Verbose mode
    parser.add_argument("--verbose", "-v", help="increase output verbosity", action="store_true")
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)
    log = logging.getLogger(__name__)

    try:
        controller = PiconController()
        if args.line_sensor:
            line_sensor = LineSensor(4)
        controller.move_stop()
    except Exception as e:
        log.error(e)
        exit()

    bd = BlueDot()

    bd.when_pressed = bluedot_move
    bd.when_moved = bluedot_move
    bd.when_released = bluedot_release

    try:
        app = default_app()
        app.run(host=args.host, port=args.port, server='tornado')
    except:
        log.error("Unable to start server on {}:{}".format(args.host, args.port))
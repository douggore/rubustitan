import cv2

# LOWER_THRESHOLD = 200
LOWER_THRESHOLD = 140

class LineProcessor:
    PreferredResolution = (320, 240)

    def __init__(self, queue):
        self.queue = queue
        self.debug = False

        self.queue.register_commands(
            {
                "visual_debug": self.toggle_debug
            })

    def __del__(self):
        self.queue.unregister_commands(["visual_debug"])

    def toggle_debug(self, _):
        self.debug = not self.debug

        if not self.debug:
            cv2.destroyAllWindows()

    def process_image(self, image):
        height, width, _ = image.shape
        mid_point_y = height // 2

        # Crop the image
        # img[y:y+h, x:x+w]
        crop_img = image[mid_point_y:height, 0:width]

        # Convert to grayscale
        gray = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)

        # Gaussian blur
        blur = cv2.GaussianBlur(gray,(5,5),0)

        # Color thresholding
        ret, thresh = cv2.threshold(blur, LOWER_THRESHOLD, 255, cv2.THRESH_BINARY)

        # Find the contours of the frame
        # imgout, contours, _ = cv2.findContours(thresh, 1, cv2.CHAIN_APPROX_NONE)
        imgout, contours, _ = cv2.findContours(thresh, 1, cv2.CHAIN_APPROX_SIMPLE)

        if len(contours) > 0:
            # print("Found {} contours".format(len(contours)))
            # print(contours)
            c = max(contours, key=cv2.contourArea)

            M = cv2.moments(c)
            # print("m10 = {}, m00 = {}".format(M['m10'], M['m00']))

            if M['m00'] > 0.0:
                cx = int(M['m10']/M['m00'])
                cy = int(M['m01']/M['m00'])

                cv2.line(crop_img,(cx,0),(cx,720),(255,0,0),1)
                cv2.line(crop_img,(0,cy),(1280,cy),(255,0,0),1)

                if self.debug:
                    cv2.drawContours(crop_img, contours, -1, (0,255,0), 1)
                    cv2.imshow("draw", crop_img)

                offset = ((cx / width) - 0.5) * 2.0
                
                # print("Offset: {}".format(offset))

                self.queue.send("line_found", { "hoffset": offset })

        # else:
        #     print("I don't see the line")

        # # print("ID = {}, centre offset = {}".format(ids[0], target_offset))

        if self.debug:
            # Find the biggest contour (if detected)
            cv2.imshow("out", thresh)
            cv2.waitKey(1)

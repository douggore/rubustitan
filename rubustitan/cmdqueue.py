from threading import Thread, Event
from queue import Queue
import inspect
import pprint

class CommandQueue():
    """Provides a command and dispatch system to communicate between components in
    a system.
    """

    def __init__(self):
        self.command_map = {}
        self.queue = Queue()
        self.thread = Thread(target=self.processor)
        self.thread_finished = Event()

    def __enter__(self):
        self.thread.start()
        return self

    def __exit__(self, *err):
        print("Start cmdqueue shutdown")
        self.send("exit", {})
        # self.thread.join()

        print("DANGLING COMMANDS:")
        pp = pprint.PrettyPrinter(indent=4)
        pp.pprint(self.command_map)

    def send(self, command, obj):
        item = {
            "command": command,
            "obj": obj
        }
        self.queue.put(item)

    def wait_for_exit(self):
        print("Wait for exit event...")
        self.thread_finished.wait()
        print("Got exit event")

    def get_calling_class(self):
        stack = inspect.stack()
        return stack[2][0].f_locals["self"].__class__

    def register_commands(self, cmds):
        """Register one or more commands and their callback functions in the system.

        Args:
            cmds (dict): Dictionary of commands and callbacks
        """
        for command, function in cmds.items():
            self.command_map.setdefault(command, []).append(
                {
                    "callback": function,
                    "owner": self.get_calling_class()
                })

    def unregister_commands(self, cmds):
        caller = self.get_calling_class()
        
        for command in cmds:
            if command in self.command_map:
                self.command_map[command][:] = [x for x in self.command_map[command] if not caller]

                if len(self.command_map[command]) == 0:
                    del self.command_map[command]

    def processor(self):
        running = True

        while running:
            # print("Cmd queue size = {}".format(self.queue.qsize()))
            item = self.queue.get()

            if item["command"] == "exit":
                print("Exit command received")
                running = False
            else:
                try:
                    for command in self.command_map[item["command"]]:
                        func = command["callback"]
                        # print(func)
                        func(item["obj"])
                except KeyError:
                    print("Command not registered:", item["command"])

            self.queue.task_done()

        print("Terminating command queue thread")
        self.thread_finished.set()

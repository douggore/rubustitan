# Controller for 4tronix PiCon Zero Intelligent Robotics Controller

from rubustitan import piconzero
from rubustitan import hcsr4_1pin

class PiconController:
    def __init__(self, command_queue):
        self.queue = command_queue

        piconzero.init()
        hcsr4_1pin.init()

        self.queue.register_commands(
            {
                "motor_speed": self.set_motor_speeds,
                "read_distance": self.read_distance
            })

    def __del__(self):
        piconzero.stop()
        hcsr4_1pin.cleanup()
        piconzero.cleanup()

    def translate(self, value, leftMin, leftMax, rightMin, rightMax):
        """Translate value from original scale to a given scale
        
        Args:
            value (TYPE): Original value
            leftMin (TYPE): Original scale minimum value
            leftMax (TYPE): Original scale maximum value
            rightMin (TYPE): New scale minimum value
            rightMax (TYPE): New scale maximum value
        
        Returns:
            TYPE: Value as calculated within the given new scale
        """
        
        # Figure out how 'wide' each range is
        leftSpan = leftMax - leftMin
        rightSpan = rightMax - rightMin

        # Convert the left range into a 0-1 range (float)
        valueScaled = float(value - leftMin) / float(leftSpan)

        # Convert the 0-1 range into a value in the right range.
        return rightMin + (valueScaled * rightSpan)

    def set_motor_speed(self, motor, speed):
        picon_speed = int(self.translate(speed, -1.0, 1.0, -128, 127))

        # Avoid stalls and thus power surges by preventing really low PWM
        if (picon_speed > 0) and (picon_speed < 30):
            picon_speed = 30
        elif (picon_speed < 0) and (picon_speed > -30):
            picon_speed = -30

        picon_speed = ~picon_speed

        # HACK: Negating 0 results in -1, fix to 0
        if picon_speed == -1:
            picon_speed = 0

        piconzero.setMotor(motor, picon_speed)

    def set_motor_speeds(self, data):
        for key, value in data.items():
            if key == "motor0":
                self.set_motor_speed(0, value)
            elif key == "motor1":
                self.set_motor_speed(1, value)

    def read_distance(self, data):
        self.queue.send("read_distance_response", {
                "distance": hcsr4_1pin.getDistance()
            })

import cv2
import json
from os import path

class ColourProcessor:
    PreferredResolution = (320, 240)

    def loadColourData(self, colour):
        filename = "{}_data.json".format(colour)
        data_path = path.abspath(path.join(path.dirname(__file__),
            "calibrations", filename))

        with open(data_path) as f:
            data = json.load(f)

        return data

    def __init__(self, queue):
        self.queue = queue
        self.debug = False
        self.ball_colour = ["red", "green", "blue", "yellow"]
        # self.ball_colour = ["yellow"]
        self.queue.register_commands({
            "set_ball_colour": self.set_ball_colour,
            "visual_debug": self.toggle_debug
        })

        data = self.loadColourData("red")
        self.redUpper = tuple(data["high"])
        self.redLower = tuple(data["low"])

        data = self.loadColourData("green")
        self.greenUpper = tuple(data["high"])
        self.greenLower = tuple(data["low"])

        data = self.loadColourData("yellow")
        self.yellowUpper = tuple(data["high"])
        self.yellowLower = tuple(data["low"])

        data = self.loadColourData("blue")
        self.blueUpper = tuple(data["high"])
        self.blueLower = tuple(data["low"])

    def __del__(self):
        self.queue.unregister_commands(["set_ball_colour", "visual_debug"])

    def toggle_debug(self, _):
        self.debug = not self.debug

        if not self.debug:
            cv2.destroyAllWindows()

    def set_ball_colour(self, data):
        print("Ball colour changed to " + data["colour"])
        self.ball_colour = [data["colour"]]

    def getContour(self, image, lowFilter, highFilter, ball_colour=""):
        mask = cv2.inRange(image, lowFilter, highFilter)
        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)

        # maskid = "{}_mask".format(ball_colour)

        # if self.debug:
        #     cv2.imshow(maskid, mask)

        cnts = cv2.findContours(mask, cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_SIMPLE)[-2]

        if len(cnts) > 0:
            # find the largest contour in the mask
            return max(cnts, key=cv2.contourArea)

        return None

    def process_image(self, image):
        # ball_colour = self.ball_colour
        height, width, _ = image.shape

        # mid_point_y = height // 2

        ycbcr = cv2.cvtColor(image, cv2.COLOR_BGR2YCrCb) 

        for ball_colour in self.ball_colour:
            # construct a mask for the color "green", then perform
            # a series of dilations and erosions to remove any small
            # blobs left in the mask
            if ball_colour == "green":
                c = self.getContour(ycbcr, self.greenLower, self.greenUpper, ball_colour)
            elif ball_colour == "red":
                c = self.getContour(ycbcr, self.redLower, self.redUpper, ball_colour)
            elif ball_colour == "blue":
                c = self.getContour(ycbcr, self.blueLower, self.blueUpper, ball_colour)
            else:
                c = self.getContour(ycbcr, self.yellowLower, self.yellowUpper, ball_colour)

            # if self.debug:
            #     output = cv2.cvtColor(image, cv2.COLOR_YCrCb2RGB) 
            #     # output = cv2.bitwise_and(image, image, mask = mask)
            #     cv2.imshow("colorout", output)

            # contours = cv2.findContours(mask, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)[-2]
            # for cnt in contours:
            #     if 200<cv2.contourArea(cnt)<5000:
            #         (x,y,w,h) = cv2.boundingRect(cnt)
            #         cv2.rectangle(image, (x,y), (x+w,y+h), (255,0,0), 2)

                    # cv2.drawContours(image,[cnt],0,(0,255,0),2)
                    # cv2.drawContours(mask,[cnt],0,255,-1)

            # find contours in the mask and initialize the current
            # (x, y) center of the ball
                
            # center = None
        
            # only proceed if at least one contour was found
            if c is not None:
                peri = cv2.arcLength(c, True)
                approx = cv2.approxPolyDP(c, 0.04 * peri, True)
                sides = len(approx)
                area = cv2.contourArea(c)

                # if len(approx) != 4:
                #     print("REJECTED")
                #     print("Approx = {}, peri = {}".format(len(approx), peri))
                    # break

                if area < 3000.0:
                    break

                M = cv2.moments(c)

                center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

            #     # only proceed if the radius meets a minimum size
            #     if 10 <= radius < 170:

                if self.debug:
                    caption = "{} ({}, {})".format(ball_colour, sides, cv2.contourArea(c))
                    # Mark the central point and draw the contour outline
                    cv2.circle(image, center, 5, (0, 0, 255), -1)
                    cv2.drawContours(image, c, -1, (0,255,0), 1)
                    font = cv2.FONT_HERSHEY_SIMPLEX
                    cv2.putText(image, caption, center, font, 0.5, (255, 255, 0), 1, cv2.LINE_AA)
                    
                target_offset = ((center[0] / width) - 0.5) * 2.0
                    
            #         # print("Send command: {} ball at {}".format(self.ball_colour, target_offset))

                self.queue.send("colour_detected", {
                    "colour": ball_colour,
                    "hoffset": target_offset,
                    "area": cv2.contourArea(c)
                })

        if self.debug:
            # cv2.imshow("hsv", hsv)
            cv2.imshow("out", image)
            cv2.waitKey(1)

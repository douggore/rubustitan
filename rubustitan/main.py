#!/usr/bin/env python3
"""Robot entry module, loads all the required submodules to function."""

import importlib
from argparse import ArgumentParser
from subprocess import call
from platform import system
from rubustitan import CommandQueue, LogicManager, SoundController, WebController, VisionController


SHUTDOWN_ON_EXIT = False

def user_options():
    parser = ArgumentParser()

    parser.add_argument("-H", "--hardware", nargs=2, metavar=("MODULE", "CLASS"),
                        help="Load the specified module and class to use as the hardware interface")

    parser.add_argument("-C", "--controller",
                        help="Load the specified manual controller module")
    parser.add_argument("--oled", action="store_true", help="Enable OLED screen")
    return parser.parse_args()

def main():
    print("Rubus TITAN")

    # System defaults
    hwcontroller_modulename = "rubuscontroller"
    hwcontroller_classname = "RubusController"
    camera_module_modulename = "picamerainterface"
    camera_module_classname = "PiCameraInterface"
    controller_modulename = "game_controller"

    # If running on a Windows PC default to emualtion mode
    if system() == "Windows":
        hwcontroller_modulename = "fakehw_controller"
        hwcontroller_classname = "FakeHwController"
        camera_module_modulename = "opencvcamerainterface"
        camera_module_classname = "OpenCvCameraInterface"
        controller_modulename = "console_controller"

    args = user_options()

    if args.hardware:
        hwcontroller_modulename, hwcontroller_classname = args.hardware

    if args.controller:
        controller_module = args.controller

    if args.oled:
        from rubustitan.oledcontroller import OledController

    hw_controller_module_path = "rubustitan.{}".format(hwcontroller_modulename)
    camera_module_path = "rubustitan.{}".format(camera_module_modulename)
    controller_module_path = "rubustitan.{}".format(controller_modulename)

    # Dynamically import required modules
    hwcontroller_module = importlib.import_module(hw_controller_module_path)
    hwcontroller_class = getattr(hwcontroller_module, hwcontroller_classname)
    camera_module = importlib.import_module(camera_module_path)
    camera_class = getattr(camera_module, camera_module_classname)
    controller_module = importlib.import_module(controller_module_path)
    controller_class = getattr(controller_module, "Controller")

    with CommandQueue() as cmd_q:
        web_controller = WebController(cmd_q)
        hw_controller = hwcontroller_class(cmd_q)
        input_controller = controller_class(cmd_q)
        sound_controller = SoundController(cmd_q)
        vision_controllor = VisionController(cmd_q, camera_class)

        if args.oled:
            oled_controller = OledController(cmd_q)

        logic_manager = LogicManager(cmd_q, "minimalmaze")

        input_controller.start()
        web_controller.start()
        sound_controller.start()

        if args.oled:
            oled_controller.start()

        logic_manager.start()

        cmd_q.wait_for_exit()

        sound_controller.play_sound("shutdown.wav")

        logic_manager.cleanup()

        if args.oled:
            oled_controller.stop()

        sound_controller.stop()
        vision_controllor.cleanup()
        input_controller.stop()
        web_controller.stop()

        hw_controller.unregister()
        print("Terminating command queue")

    print("Robot terminating")

    if SHUTDOWN_ON_EXIT:
        print("Power down")
        call("sudo shutdown -h now", shell=True)

if __name__ == '__main__':
    from gevent import monkey
    monkey.patch_all()
    main()

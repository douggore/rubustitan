# Controller for the game controller class

from time import sleep
from threading import Thread

def _find_getch_kbhit():
    try:
        import termios
    except ImportError:
        # Non-POSIX. Return msvcrt's (Windows') getch.
        import msvcrt
        return msvcrt.getch, msvcrt.kbhit

    # POSIX system. Create and return a getch that manipulates the tty.
    import sys, tty
    def _getch():
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(fd)
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch

    def _kbhit():
        pass

    return _getch, _kbhit

getch, kbhit = _find_getch_kbhit()

class Controller():
    def __init__(self, command_queue):
        self.thread = Thread(target=self.thread_main)
        self.queue = command_queue
        self.running = False

    def start(self):
        self.thread.start()

    def stop(self):
        print("Stop controller thread")
        self.running = False
        self.thread.join()
        print("Stopped controller thread")

    def thread_main(self):
        self.running = True

        while self.running:
            if kbhit():
                input_char = getch()
                print("Getch: ", input_char)

                if input_char == b'q':
                    self.queue.send("exit", {})
                elif input_char == b'[':
                    self.queue.send("cycle_mode", {})
                elif input_char == b']':
                    self.queue.send("button_presses", ["start"])
                elif input_char == b'\xe0':
                    input_char = getch()
                    x_axis = 0.0
                    throttle = 0.0

                    if input_char == b'H': # Up
                        throttle = 1.0
                    elif input_char == b'M': # Right
                        throttle = 1.0
                        x_axis = 1.0
                    elif input_char == b'P': # Down
                        throttle = -1.0
                    elif input_char == b'K': # Left
                        throttle = 1.0
                        x_axis = -1.0

                    self.queue.send("operator_input",
                        {
                            "yaw": x_axis,
                            "throttle": throttle
                        })
            else:
                sleep(0.1)

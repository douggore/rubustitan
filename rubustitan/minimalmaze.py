# manualdrive.py
from time import sleep
from threading import Thread, Event
from rubustitan import MotorMixer

CUTOFF_DISTANCE = 25

CARPET = True

if CARPET:
    SCAN_SPEED = 0.6
    # CENTRE_SPEED = 0.4
else:
    SCAN_SPEED = 0.5
    # CENTRE_SPEED = 0.2

class MinimalMaze:
    def __init__(self, command_queue):
        self.running = False
        self.queue = command_queue

        self.activate = False
        self.distance_notified = Event()
        self.distance = 0.0

        self.marker_notified = Event()
        self.marker_id = 0
        self.offset = 0.0
        self.area = 0.0

        self.expected_marker = 0

    def enter(self):
        print("Enter MinimalMaze")

        self.queue.register_commands(
            {
                "read_distance_response": self.distance_read,
                "button_presses": self.buttons_pressed,
                "armarker": self.marker_found
            })

        self.queue.send("start_camera", {
            "processor": "armarker"
        })

        self.thread = Thread(target=self.thread_main)
        self.thread.start()

    def exit(self):
        print("Exit MinimalMaze")

        self.running = False
        self.thread.join()

        self.queue.unregister_commands(["read_distance_response", "button_presses", "armarker"])
        self.queue.send("stop_camera", {})        

    def drive_with_rangefinder(self, drive_power=0.7):
        print("Drive with range finder at power level {}".format(drive_power))

        print("Get initial distance measurement")
        self.queue.send("read_distance", {})
        self.distance_notified.wait()
        self.distance_notified.clear()

        distance_left = self.distance

        self.queue.send("motor_speed",
                        {
                            "motor0": drive_power,
                            "motor1": drive_power
                        })

        print("Enter range checking loop")
        while self.activate and distance_left > CUTOFF_DISTANCE:
            self.queue.send("read_distance", {})
            self.distance_notified.wait()
            self.distance_notified.clear()

            if self.distance < distance_left:
                print("Moving distance to obj:", self.distance)
                distance_left = self.distance
            else:
                print("Throw away outlier value of {}".format(self.distance))

        self.queue.send("motor_speed",
                        {
                            "motor0": 0.0,
                            "motor1": 0.0
                        })

        print("Target distance now:", self.distance)
        print("Cut power to motors")

    # def marker_distance(self, obj_height):
    #     # V1 camera
    #     # SENSOR_DIMENSIONS = ( 3.76, 2.74 )
    #     SENSOR_HEIGHT = 2.74
    #     FOCAL_LENGTH = 3.60
    #     MARKER_HEIGHT = 66
    #     IMAGE_HEIGHT = 480

    #     return (FOCAL_LENGTH * MARKER_HEIGHT * IMAGE_HEIGHT) / (obj_height * SENSOR_HEIGHT)

    def drive_to_marker(self, drive_power=0.5):
        active = True

        print("Drive to marker")

        while self.activate and active:
            self.marker_notified.wait(0.2)

            if self.marker_notified.is_set():
                self.marker_notified.clear()

                if self.area < 0.15:
                    power_left, power_right = MotorMixer.swing_turn(-self.offset, drive_power)

                    self.queue.send("motor_speed",
                                    {
                                        "motor0": power_left,
                                        "motor1": power_right
                                    })

                    print("Motor L: {}, R: {}".format(power_left, power_right))

                    # self.queue.send("motor_speed",
                    #     {
                    #         "motor0": drive_power,
                    #         "motor1": drive_power
                    #     })
                else:
                    active = False
            else:
                print("Cut power until we have a fix")
                self.queue.send("motor_speed",
                    {
                        "motor0": 0.0,
                        "motor1": 0.0
                    })
                
        print("Marker close, deactivate")
        self.queue.send("motor_speed",
            {
                "motor0": 0.0,
                "motor1": 0.0
            })

    def turn_to_marker(self, marker_id, scan_speed=0.8):
        active = True
        motor_on = True
        error_left = 0.0
        error_right = 0.0

        print("Turn to marker {}".format(marker_id))

        drive_left, drive_right = MotorMixer.point_turn(-scan_speed)

        self.queue.send("motor_speed",
            {
                "motor0": drive_left,
                "motor1": drive_right
            })

        while self.activate and active:
            self.marker_notified.wait(0.2)

            if self.marker_notified.is_set():
                self.marker_notified.clear()

                if self.marker_id == marker_id:
                    print("Seen target marker {} at offset {}".format(marker_id, self.offset))
                    
                    if self.offset > 0.1:
                        error_left = 0.0
                        error_right += 0.01

                        print("Target to right, turn right")
                        # drive_left, drive_right = MotorMixer.point_turn(-self.offset)
                        drive_left, drive_right = MotorMixer.point_turn(-1 * min(self.offset + error_right, scan_speed))
                        
                        self.queue.send("motor_speed",
                            {
                                "motor0": drive_left,
                                "motor1": drive_right
                            })
                    elif self.offset < -0.1:
                        error_right = 0.0
                        error_left += 0.01

                        print("Target to left, turn left")
                        # drive_left, drive_right = MotorMixer.point_turn(-self.offset)
                        drive_left, drive_right = MotorMixer.point_turn(min(self.offset + error_left, scan_speed))
                        self.queue.send("motor_speed",
                            {
                                "motor0": drive_left,
                                "motor1": drive_right
                            })
                    else:
                        print("Target central, stop")
                        active = False
            # else:
                # print("ERROR: Marker no longer visible")
                # print("Invert power state")
                # motor_on = not motor_on
                # self.queue.send("motor_speed",
                #     {
                #         "motor0": drive_left if motor_on else 0.0,
                #         "motor1": drive_right if motor_on else 0.0
                #     })
                
        print("Marker found and aligned, deactivate")
        self.queue.send("motor_speed",
            {
                "motor0": 0.0,
                "motor1": 0.0
            })

    def thread_main(self):
        self.running = True

        while self.running:
            if self.activate:
                self.queue.send("play_sound", { "sound": "activate" })

                print("Begin ultrasonic lead in sequence")
                self.drive_with_rangefinder(0.5)

                print("Begin marker based navigation")
                self.expected_marker = 0
                while self.activate:
                    self.turn_to_marker(self.expected_marker, SCAN_SPEED)
                    self.drive_to_marker()
                    self.expected_marker += 1

                    if self.marker_id == 5:
                        self.activate = False

                self.queue.send("play_sound", { "sound": "complete" })
            else:
                sleep(1)

    def buttons_pressed(self, data):
        print(data)
        if "start" in data:
            self.activate = not self.activate
            print("Autonomous activate?", self.activate)
        elif "circle" in data:
            self.queue.send("visual_debug", {})

    def distance_read(self, data):
        self.distance = data["front"]
        self.distance_notified.set()

    def marker_found(self, data):
        print("Marker id = {}, hoffset = {}, area = {}".format(data["id"], data["hoffset"], data["area"]));
        
        if data["id"] == self.expected_marker:
            self.marker_id = data["id"]
            self.offset = data["hoffset"]
            self.area = data["area"]
            self.marker_notified.set()
        else:
            print("Discard this marker")

from threading import Thread
import cv2

class OpenCvCameraInterface:
    def __init__(self):
        self.capture = None
        self.callback = None
        self.thread = Thread(target=self.thread_main)
        
    def start_capture(self, callback, resolution=(320, 240), framerate=3):
        width, height = resolution

        self.callback = callback
        self.capture = cv2.VideoCapture(0)
        self.capture.set(cv2.CAP_PROP_FRAME_WIDTH, width)
        self.capture.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
        self.capture.set(cv2.CAP_PROP_FRAME_COUNT, framerate)
        self.thread.start()

    def stop_capture(self):
        self.capture.release()

    def thread_main(self):
        while self.capture.isOpened():
            ret, frame = self.capture.read()

            if ret:
                # cv2.imshow("out", frame)
                cv2.waitKey(1)
                self.callback(frame)

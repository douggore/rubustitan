from rubustitan import ManualDrive

class DuckShoot(ManualDrive):
    def __init__(self, queue):
        super(DuckShoot, self).__init__(queue)
        self.queue = queue
        self.target_on = False

    def enter(self):
        super(DuckShoot, self).enter()

        self.queue.register_commands(
            {
                "button_presses": self.buttons_pressed,
                "projectile_launched": self.projectile_launched
            })

    def exit(self):
        super(DuckShoot, self).exit()
        self.queue.unregister_commands(["button_presses", "projectile_launched"])

    def buttons_pressed(self, data):
        if "square" in data:
            print("FIRE!")
            
            self.queue.send("screen", { "action": "animation", "data": {"file": "Squint.gif"} })
            self.queue.send("screen", { "action": "animation", "data": {"file": "FocusedAlt.gif", "loop": True} })
            self.queue.send("projectile", {})
        
        if "cross" in data:
            print("LASER TARGET ON/OFF")
            self.target_on = not self.target_on

            self.queue.send("laser", { "on": self.target_on })

    def projectile_launched(self, data):
        self.queue.send("screen", { "action": "animation", "data": {"file": "Unsquint.gif"} })
        self.queue.send("screen", { "action": "animation", "data": {"file": "Idle.gif", "loop": True} })
        # pass
import cv2

# Original
# greenLower = (29, 86, 6)
# greenUpper = (74, 255, 255)

# HSV Picker under lights
greenLower = (42, 50, 28)
greenUpper = (80, 255, 255)

# Original
# redLower = (160, 50, 50)
# redUpper = (179, 255, 255)

# HSV Picker under lights
redLower = (0, 0, 26)
redUpper = (10, 255, 255)

# Original
# blueLower = (80, 50, 50)
# blueUpper = (120, 255, 255)

# HSV Picker under lights
blueLower = (64, 0, 10)
blueUpper = (140, 255, 255)

# Original
# yellowLower = (18, 50, 50)
# yellowUpper = (32, 255, 255)

# HSV Picker under lights
yellowLower = (22, 0, 76)
yellowUpper = (33, 255, 255)

class BallProcessor:
    PreferredResolution = (320, 240)

    def __init__(self, queue):
        self.queue = queue
        self.debug = False
        self.ball_colour = "green"
        self.queue.register_commands({
            "set_ball_colour": self.set_ball_colour,
            "visual_debug": self.toggle_debug
        })

    def __del__(self):
        self.queue.unregister_commands(["set_ball_colour", "visual_debug"])

    def toggle_debug(self, _):
        self.debug = not self.debug

        if not self.debug:
            cv2.destroyAllWindows()

    def set_ball_colour(self, data):
        print("Ball colour changed to " + data["colour"])
        self.ball_colour = data["colour"]

    def process_image(self, image):
        ball_colour = self.ball_colour
        height, width, _ = image.shape

        mid_point_y = height // 2

        # Crop the image
        # img[y:y+h, x:x+w]
        image = image[0:mid_point_y, 0:width]

        hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV) 

        # construct a mask for the color "green", then perform
        # a series of dilations and erosions to remove any small
        # blobs left in the mask
        if ball_colour == "green":
            mask = cv2.inRange(hsv, greenLower, greenUpper)
        elif ball_colour == "red":
            mask = cv2.inRange(hsv, redLower, redUpper)
        elif ball_colour == "blue":
            mask = cv2.inRange(hsv, blueLower, blueUpper)
        else:
            mask = cv2.inRange(hsv, yellowLower, yellowUpper)

        mask = cv2.erode(mask, None, iterations=2)
        mask = cv2.dilate(mask, None, iterations=2)
        
        # find contours in the mask and initialize the current
        # (x, y) center of the ball
        cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
            cv2.CHAIN_APPROX_SIMPLE)[-2]
            
        center = None
    
        # only proceed if at least one contour was found
        if len(cnts) > 0:
            # find the largest contour in the mask, then use
            # it to compute the minimum enclosing circle and
            # centroid
            c = max(cnts, key=cv2.contourArea)
            ((x, y), radius) = cv2.minEnclosingCircle(c)
            M = cv2.moments(c)
            center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

            # only proceed if the radius meets a minimum size
            if 10 <= radius < 170:

                if self.debug:
                    # draw the circle and centroid on the frame,
                    # then update the list of tracked points
                    cv2.circle(image, (int(x), int(y)), int(radius),
                        (0, 255, 255), 2)
                    cv2.circle(image, center, 5, (0, 0, 255), -1)
                
                target_offset = ((center[0] / width) - 0.5) * 2.0
                
                # print("Send command: {} ball at {}".format(self.ball_colour, target_offset))

                self.queue.send("colouredball", {
                    "colour": ball_colour,
                    "hoffset": target_offset,
                    "radius": radius
                })

        # update the points queue
        # pts.appendleft(center)

        if self.debug:
            # cv2.imshow("hsv", hsv)
            cv2.imshow("mask", mask)
            cv2.imshow("out", image)
            cv2.waitKey(1)

from time import sleep
from enum import IntEnum
import sys

try:
    import smbus
except:
    import fakebus as smbus

i2c_bus = smbus.SMBus(1)

class InterruptStatus(IntEnum):
    """Summary
 
    Attributes:
        FAULT (int): Fault detection interrupt
        POWER_ON (int): Power on interrupt
        SENSOR (int): Sensor event interrupt
    """

    POWER_DOWN = 5
    LOW_BATTERY = 6
    FAULT = 7

class Sensors(IntEnum):
    SIX_AXIS = 0

class FaultStatus(IntEnum):
    LOW_BATTERY = 0
    BATTERY_CHARGING = 1
    POWER_INPUT = 2
    POWER_OUTPUT = 3

# Default I2C address of the microcontroller
MSP430_ADDR = 0x18

# List of microcontroller registers
REG_LEFT_MOTOR = 0x00
REG_RIGHT_MOTOR = 0x01
REG_CONTROL = 0x02
REG_LION_ADC_HIGH = 0x03
REG_LION_ADC_LOW = 0x04
REG_NIMH_ADC_HIGH = 0x05
REG_NIMH_ADC_LOW = 0x06
REG_SERVO_CONTROL = 0x07
REG_WHITE_LED_CONTROL = 0x08
REG_FIRMWARE_VERSION = 0x09

BATTERY_VOLTAGE_MULTIPLIER = 0.0024437

def write_mcu(register, value):
    print("Writing 0x{value:02x} ({value:0b}) to register 0x{reg:02x}".format(value=value, reg=register))
    try:
        if type(value) is list:
            i2c_bus.write_i2c_block_data(MSP430_ADDR, register, value)
        else:
            i2c_bus.write_byte_data(MSP430_ADDR, register, value)
    except OSError:
        print("Error reading data from the control board")
        print(" - Ensure the control board is connected to the Raspberry Pi")
        print(" - Check the connections to ensure they have not come loose")
        # sys.exit()

def read_mcu(register, length=1):
    try:
        if length == 1:
            return i2c_bus.read_byte_data(MSP430_ADDR, register)
        else:
            return i2c_bus.read_i2c_block_data(MSP430_ADDR, register, length)
    except OSError:
        print("Error reading data from the control board")
        print(" - Ensure the control board is connected to the Raspberry Pi")
        print(" - Check the connections to ensure they have not come loose")
        # sys.exit()

def clear_bit(register, bit):
    """Clear a bit on a register
    
    Args:
        register (int): Register to modify
        bit (int): Index of the bit to clear
    """
    register_value = read_mcu(register)
    register_value &= ~(1 << bit) & 0xFF
    write_mcu(register, register_value)

def set_bit(register, bit):
    """Set a bit on a register
    
    Args:
        register (int): Register to modify
        bit (int): Index of the bit to set
    """
    register_value = read_mcu(register)
    register_value &= ~(1 << bit) & 0xFF
    register_value |= 1 << bit
    write_mcu(register, register_value)

# def read_status():
#     """Read the MCU status regsiter

#     Returns:
#         List: Returns a list of interrupt reasons encountered
#     """
#     status = read_mcu(REG_STATUS)
#     result = []

#     for bit in InterruptStatus:
#         if status & (1 << bit):
#             result.append(InterruptStatus(bit))

#     return result

# def read_fault_status():
#     fault_status = read_mcu(REG_FAULT_STATUS)
#     result = []

#     for bit in range(0, 3):
#         if fault_status & (1 << bit):
#             result.append(FaultStatus(bit))

#     return result

def firmware_version():
    """Get the MCU firmware version

    Returns:
        int: Firmware version from MCU
    """
    return read_mcu(REG_FIRMWARE_VERSION) / 10

def read_adc_voltage(high_register):
    battery_buffer = [0, 0]
    battery_buffer = read_mcu(high_register, 2)
    adc_high_byte = battery_buffer[0] & 0x03
    adc_low_byte = battery_buffer[1]
    adc = (adc_high_byte << 8) | adc_low_byte
    voltage = (adc * BATTERY_VOLTAGE_MULTIPLIER) / 0.1755
    return voltage

def battery_voltages():
    set_bit(REG_CONTROL, 7)
    return (read_adc_voltage(REG_LION_ADC_HIGH), read_adc_voltage(REG_NIMH_ADC_HIGH))

# def lion_battery_voltage():
#     return read_adc_voltage(REG_LION_ADC_HIGH)

# def nimh_battery_voltage():
#     return read_adc_voltage(REG_NIMH_ADC_HIGH)

def battery_voltage_as_percent(voltage):
    min_voltage = 1.96
    max_voltage = 4.33
    voltage_increment = (max_voltage - min_voltage) / 100

    percentage = (voltage - min_voltage) / voltage_increment
    return percentage

def enable_servo():
    set_bit(REG_SERVO_CONTROL, 7)

def disable_servo():
    clear_bit(REG_SERVO_CONTROL, 7)

def set_servo_rotation(angle):
    servo_control = read_mcu(REG_SERVO_CONTROL)
    servo_control &= 0x80
    servo_control |= angle & 0x7F
    write_mcu(REG_SERVO_CONTROL, servo_control)

def enable_headlights():
    set_bit(REG_WHITE_LED_CONTROL, 7)

def disable_headlights():
    clear_bit(REG_WHITE_LED_CONTROL, 7)

def set_headlight_brightness(brightness):
    led_control = read_mcu(REG_WHITE_LED_CONTROL)
    led_control &= 0x80
    led_control |= brightness & 0x7F
    write_mcu(REG_WHITE_LED_CONTROL, led_control)

def set_motor(reg, forwards, speed):
    motor_control = 0x00

    if forwards:
        motor_control |= 0x80

    motor_control |= speed & 0x7F
    write_mcu(reg, motor_control)

def set_left_motor(forwards, speed):
    set_motor(REG_LEFT_MOTOR, forwards, speed)
    
def set_right_motor(forwards, speed):
    set_motor(REG_RIGHT_MOTOR, forwards, speed)

def enable_relay():
    set_bit(REG_CONTROL, 6)

def disable_relay():
    clear_bit(REG_CONTROL, 6)
# manualdrive.py
import time
from time import sleep
from threading import Thread, Event
from rubustitan import MotorMixer

CUTOFF_DISTANCE = 20

# navigation = [
#     # Drive to first corner (top left)
#     {
#         "action": "drive",
#         "front": 10
#     },
#     # Turn to second corner (top right)
#     {
#         "action": "turn",
#         "direction": "right",
#         "front": 100,
#         "rear": 20,
#         "marker": 0
#     },
#     # Drive to second corner (before the 45 wall)
#     {
#         "action": "drive",
#         "front": 40
#     },
#     # Turn towards bottom of maze
#     {
#         "action": "turn",
#         "direction": "right",
#         "front": 50,
#         "rear": 20,
#         "marker": 1
#     },
#     # Drive to centre of maze
#     {
#         "action": "drive",
#         "front": 10,
#     },
#     # Turn to left side of the maze
#     {
#         "action": "turn",
#         "direction": "right",
#         "front": 70,
#         "back": 20,
#         "marker": 2
#     },
#     # Move to left side
#     {
#         "action": "drive",
#         "front": 20
#     },
#     # Turn to bottom of the maze
#     {
#         "action": "turn",
#         "direction": "left",
#         "front": 30,
#         "back": 20,
#         "marker": 3
#     },
#     # Drive to bottom of maze
#     {
#         "action": "drive",
#         "front": 20
#     },
#     # Turn to right of maze
#     {
#         "action": "turn",
#         "direction": "left",
#         "front": 110,
#         "back": 50,
#         "marker": 4
#     },
#     # Drive to right hand side
#     {
#         "action": "drive",
#         "front": 20
#     },
#     # Turn to top of maze (exit)
#     {
#         "action": "turn",
#         "direction": "left",
#         "front": 100,
#         "back": 20,
#         "marker": 5
#     },
#     # Drive to exit
#     {
#         "action": "drive"
#     }
# ]

navigation = [
    "left",
    "left",
    "right",
    "right",
    "left",
    "left",
    "left",
    "right"
]

class MarsMaze:
    def __init__(self, command_queue):
        self.queue = command_queue

        self.thread = None
        self.running = False

        self.activate = False

        self.distance_notified = Event()
        self.heading_notified = Event()
        self.front_distance = 0.0
        self.rear_distance = 0.0
        self.heading = 0.0

    def enter(self):
        print("Enter Mars Maze (ultrasonic)")

        self.queue.register_commands(
            {
                "button_presses": self.buttons_pressed,
                "read_distance_response": self.distance_read,
                "read_heading_response": self.heading_read
            })

        self.thread = Thread(target=self.thread_main)
        self.thread.start()

        # self.queue.send("start_camera", {
        #     "processor": "armarker"
        # })

    def exit(self):
        print("Exit Mars Maze")
        self.activate = False
        self.running = False

        # Set the event in case the thead is blocking
        self.distance_notified.set()

        self.thread.join()

        self.queue.unregister_commands(["button_presses", "read_distance_response", "read_heading_response"])
        # self.queue.send("stop_camera", {})

    def thread_main(self):
        self.running = True

        while self.running:
            if self.activate:
                self.queue.send("play_sound", { "sound": "activate" })

                # self.drive_with_rangefinder(stop_distance=35, drive_power=0.5)
                # self.turn_right()

                nav_iter = iter(navigation)

                while self.activate:
                    nav_step = next(nav_iter, None)

                    if nav_step is None:
                        print("Navigation complete")
                        break

                    print("Next navigational direction is {}".format(nav_step))

                    self.drive_with_rangefinder(stop_distance=35, drive_power=0.5)

                    if nav_step == "left":
                        self.turn_left()
                    else:
                        self.turn_right()

                self.queue.send("motor_speed",
                                {
                                    "motor0": 0.7,
                                    "motor1": 0.7,
                                    "pid": True
                                })

                sleep(2)

                self.queue.send("motor_speed",
                                {
                                    "motor0": 0.0,
                                    "motor1": 0.0
                                })

                self.queue.send("play_sound", { "sound": "complete" })
                self.activate = False
            else:
                sleep(1)

        print("Maze thread terminating")

    def buttons_pressed(self, data):
        print("buttons = {}".format(data))
        if "start" in data:
            self.activate = not self.activate
            print("Autonomous activate =", self.activate)
        elif "circle" in data:
            self.queue.send("visual_debug", {})

    def distance_read(self, data):
        # print("Front: {}".format(data["front"]))
        # print("Front: {} {}, rear: {} {}".format(data["front"], self.front_distance, data["rear"], self.rear_distance))

        self.front_distance = data["front"]
        # self.rear_distance = data["rear"]
        self.distance_notified.set()

    def heading_read(self, data):
        self.heading = data["heading"]
        self.heading_notified.set()

    def drive_with_rangefinder(self, stop_distance=CUTOFF_DISTANCE, drive_power=0.7):
        print("Drive with range finder at power level {}".format(drive_power))

        print("Get initial distance measurement")
        self.queue.send("read_distance", {})
        self.distance_notified.wait()
        self.distance_notified.clear()

        distance_left = self.front_distance
        print("Distance = {}".format(distance_left))

        self.queue.send("motor_speed",
                        {
                            "motor0": drive_power,
                            "motor1": drive_power,
                            "pid": True
                        })

        print("Enter range checking loop with distance = {} cm".format(distance_left))
        while self.activate and distance_left > stop_distance:
            self.queue.send("read_distance", {})
            self.distance_notified.wait()
            self.distance_notified.clear()

            if self.front_distance != distance_left:
                print("Distance now:", self.front_distance)

            distance_left = self.front_distance

            # if self.front_distance < distance_left:
            #     print("Moving distance to obj:", self.front_distance)
            #     distance_left = self.front_distance
            # else:
            #     print("Throw away outlier value of {}".format(self.front_distance))

        self.queue.send("motor_speed",
                        {
                            "motor0": -0.1,
                            "motor1": -0.1
                        })

        sleep(0.1)

        self.queue.send("motor_speed",
                        {
                            "motor0": 0.0,
                            "motor1": 0.0
                        })

        self.queue.send("read_distance", {})
        self.distance_notified.wait()
        self.distance_notified.clear()

        print("Target distance now:", self.front_distance)
        print("Cut power to motors")

    def turn_to_target(self, target_heading):
        start_time = time.time()
        end_time = start_time

        while self.activate:
            delta = self.calc_angle_distance(self.heading, target_heading)
            # print("Target = {:.1f}, actual = {:.1f}, delta = {:.1f}".format(target_heading, self.heading, delta))
            motor_scale = 0.5 + (((90.0 - delta) / 90.0) * 0.4)
            # print("Motor scale = {:.2f}".format(motor_scale))
            # sleep(10)

            if -0.5 <= delta <= 0.5:
                print("Target angle met, finish, actual = {:.1f}".format(target_heading))
                break
            elif delta > 0.5:
                self.queue.send("motor_speed",
                                {
                                    "motor0": motor_scale,
                                    "motor1": -motor_scale
                                })
            else:
                self.queue.send("motor_speed",
                                {
                                    "motor0": -motor_scale,
                                    "motor1": motor_scale
                                })

            self.queue.send("read_heading", {})
            self.heading_notified.wait()
            self.heading_notified.clear()

            end_time = time.time()
            
            if end_time - start_time > 0.2:
                print("Turning, heading now = {:.1f} deg".format(self.heading))
                start_time = end_time


        self.queue.send("motor_speed",
                        {
                            "motor0": 0.0,
                            "motor1": 0.0
                        })

        self.queue.send("read_heading", {})
        self.heading_notified.wait()
        self.heading_notified.clear()

        print("Final heading = {:.1f} deg".format(self.heading))

    def turn_right(self):
        print("Turn right")
        self.queue.send("read_heading", {})
        self.heading_notified.wait()
        self.heading_notified.clear()

        initial_heading = self.heading
        target_heading = self.calc_new_angle(self.heading, 90)
        print("Initial heading = {:.1f} deg, target = {:.1f}".format(initial_heading, target_heading))

        self.queue.send("motor_speed",
                        {
                            "motor0": 0.9,
                            "motor1": -0.9
                        })

        self.turn_to_target(target_heading)

    def turn_left(self):
        print("Turn left")
        self.queue.send("read_heading", {})
        self.heading_notified.wait()
        self.heading_notified.clear()

        initial_heading = self.heading
        target_heading = self.calc_new_angle(self.heading, -90)
        print("Initial heading = {:.1f} deg, target = {:.1f}".format(initial_heading, target_heading))

        self.queue.send("motor_speed",
                        {
                            "motor0": -0.9,
                            "motor1": 0.9
                        })

        self.turn_to_target(target_heading)

    def turn_to_position(self, step, drive_power=0.5):
        turn_active = True

        print("Turn to new position")

        power_left, power_right = MotorMixer.point_turn(drive_power if step["direction"] == "right" else -drive_power)

        # Set the rotation direction
        self.queue.send("motor_speed",
                        {
                            "motor0": power_left,
                            "motor1": power_right,
                        })

        front = self.front_distance
        rear = self.rear_distance

        while turn_active and self.activate:
            self.queue.send("read_distance", {})
            self.distance_notified.wait()
            self.distance_notified.clear()

            if self.front_distance != front or self.rear_distance != rear:
                print("Distance, front = {} cm, rear = {} cm".format(self.front_distance, self.rear_distance))

            if self.front_distance >= step["front"] and self.rear_distance <= step["rear"]:
                print("STOP: Turn step criteria met!")
                turn_active = False

        self.queue.send("motor_speed",
                        {
                            "motor0": 0.0,
                            "motor1": 0.0
                        })
        
    def calc_new_angle(self, angle, delta):
        angle += delta

        if angle < 0.0:
            angle += 360.0
        elif angle > 360.0:
            angle -= 360.0

        return angle

    def calc_angle_distance(self, first_angle, second_angle):
        difference = second_angle - first_angle
        while difference < -180:
            difference += 360

        while difference > 180:
            difference -= 360

        return difference

    # def calc_angle_distance(self, alpha, beta):
    #     # This is either the distance or 360 - distance
    #     phi = abs(beta - alpha) % 360
        
    #     distance = 360 - phi if phi > 180 else phi

    #     # calculate sign 
    #     sign = (alpha - beta >= 0 && alph - beta <= 180) || (alpha - beta <=-180 && alpha - beta >= -360) ? 1 : -1; 
    #     r *= sign;
    #     return distance

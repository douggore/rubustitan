# manualdrive.py
from time import sleep
from threading import Thread, Event
from rubustitan import MotorMixer

MAX_AREA = 60000

CARPET = False

if CARPET:
    SCAN_SPEED = 0.5
    CENTRE_SPEED = 0.4
else:
    # SCAN_SPEED = 0.5
    # CENTRE_SPEED = 0.2
    SCAN_SPEED = 0.43
    CENTRE_SPEED = 0.3

# Algorithm:
# 1. Scan for ball
# 1a. Align 
# 2. Drive to ball
# 3. Signal stage complete
# 4. Reverse to 50cm
# 5. Goto 1.

class Rainbow:
    def __init__(self, command_queue):
        self.running = False
        self.queue = command_queue
        self.thread = None

        self.headlights = False

        self.activate = False
        self.zone_visited = False
        self.ball_notified = Event()
        self.ball_offset = 0.0
        self.mass_area = 0.0

        # Official
        # self.ball_order = ["blue", "yellow", "green", "red"]
        self.ball_order = ["red", "blue", "yellow", "green"]

    def enter(self):
        print("Enter Rainbow")
        self.queue.register_commands(
            {
                "button_presses": self.buttons_pressed,
                "colour_detected": self.ball_seen
            })

        self.thread = Thread(target=self.thread_main)
        self.thread.start()

        self.queue.send("start_camera", {
            "processor": "colour"
        })

        self.queue.send("set_ball_colour",
            {
                "colour": "red"
            })

    def exit(self):
        print("Exit Rainbow")
        self.activate = False
        self.running = False
        self.thread.join()

        self.queue.unregister_commands(["button_presses", "colour_detected", "set_ball_colour"])
        self.queue.send("stop_camera", {})

    def drive_to_object(self, drive_power=0.3):
        print("Move towards target")
        active = True
        self.zone_visited = False

        while not self.zone_visited and self.activate:
            if self.ball_notified.wait(0.2):
                self.ball_notified.clear()
                power_left, power_right = self.mixer(-self.ball_offset, drive_power)

                self.queue.send("motor_speed",
                                {
                                    "motor0": power_left,
                                    "motor1": power_right
                                })

                print("Motor L: {}, R: {}".format(power_left, power_right))
            else:
                self.queue.send("motor_speed",
                                {
                                    "motor0": 0.0,
                                    "motor1": 0.0
                                })

            print("Colour mass area = {}".format(self.mass_area))
            # active = self.mass_area < MAX_AREA

        print("Cut power to motors, in 0.1s")
        # TEMP HACK: drive on a little bit
        # sleep(0.6)

        self.queue.send("motor_speed",
                        {
                            "motor0": 0.0,
                            "motor1": 0.0
                        })

    def return_to_centre(self, drive_power=0.6, cutoff_radius=35):
        print("Return to centre of arena")
        # Reverse approx 50cm
        # active = True

        if not self.activate:
            return

        self.queue.send("motor_speed",
                        {
                            "motor0": -drive_power,
                            "motor1": -drive_power
                        })

        sleep(1.0)

        self.queue.send("motor_speed",
                        {
                            "motor0": 0,
                            "motor1": 0
                        })

        # while active and self.activate:
        #     if self.ball_notified.wait(0.2):
        #         self.ball_notified.clear()

        #         self.queue.send("motor_speed",
        #                         {
        #                             "motor0": -drive_power,
        #                             "motor1": -drive_power
        #                         })
        #     else:
        #         self.queue.send("motor_speed",
        #                         {
        #                             "motor0": 0.0,
        #                             "motor1": 0.0
        #                         })

        #     print("Ball radius:", self.ball_radius)
        #     active = self.ball_radius > cutoff_radius

        print("Target area now:", self.mass_area)
        print("Cut power to motors")

        self.queue.send("motor_speed",
                        {
                            "motor0": 0.0,
                            "motor1": 0.0
                        })

    def find_and_centre_ball(self, ball, scan_speed=1.0):
        active = True
        motor_on = True
        error_left = 0.0
        error_right = 0.0

        print("Turn to ball {}".format(ball))

        drive_left, drive_right = MotorMixer.point_turn(-scan_speed)

        self.queue.send("motor_speed",
                        {
                            "motor0": drive_left,
                            "motor1": drive_right
                        })

        while self.activate and active:
            self.ball_notified.wait(0.15)

            if self.ball_notified.is_set():
                self.ball_notified.clear()

                print("Seen {} ball at offset {}".format(ball, self.ball_offset))
                
                if self.ball_offset > 0.1:
                    error_left = 0.0
                    error_right += 0.01
                    print("Ball to right, turn right")
                    # drive_left, drive_right = MotorMixer.point_turn(-scan_speed)
                    drive_left, drive_right = MotorMixer.point_turn(-1.0 * min(self.ball_offset + error_right, scan_speed))
                    
                    self.queue.send("motor_speed",
                        {
                            "motor0": drive_left,
                            "motor1": drive_right
                        })
                elif self.ball_offset < -0.1:
                    error_right = 0.0
                    error_left += 0.01
                    print("Ball to left, turn left")
                    # drive_left, drive_right = MotorMixer.point_turn(scan_speed)
                    drive_left, drive_right = MotorMixer.point_turn(min(self.ball_offset + error_left, scan_speed))
                    self.queue.send("motor_speed",
                        {
                            "motor0": drive_left,
                            "motor1": drive_right
                        })
                else:
                    print("Ball central, stop")
                    active = False
            else:
                # print("ERROR: Marker no longer visible")
                # print("Invert power state")
                drive_left, drive_right = MotorMixer.point_turn(-scan_speed)
                
                # motor_on = not motor_on
                self.queue.send("motor_speed",
                    {
                        "motor0": drive_left if motor_on else 0.0,
                        "motor1": drive_right if motor_on else 0.0
                    })
                
        print("Ball found and aligned, deactivate")
        self.queue.send("motor_speed",
            {
                "motor0": 0.0,
                "motor1": 0.0
            })

        return True
    
    def thread_main(self):
        self.running = True

        while self.running:
            if self.activate:
                self.queue.send("play_sound", { "sound": "activate" })

                # Iterate over the balls in their predetermined order
                for ball in self.ball_order:
                    search_count = 3

                    self.ball_notified.clear()
                    self.ball_offset = 0.0
                    self.mass_area = 0.0

                    self.queue.send("set_ball_colour",
                                {
                                    "colour": ball
                                })

                    # We will retry our scan up to 3 times to find the target ball
                    while self.activate and search_count > 0:
                        print("Homing in on {} ball, search cycles remaining {}".format(ball, search_count))

                        success = self.find_and_centre_ball(ball, SCAN_SPEED)

                        if success:
                            break

                        search_count -= 1

                    self.drive_to_object(drive_power=0.6)

                    sleep(1)
                    self.return_to_centre()

                    # Break out of the loop if the user is trying to exit
                    if self.activate == False:
                        break

                self.activate = False
                self.queue.send("play_sound", { "sound": "complete" })
            else:
                sleep(1)

    @staticmethod
    def mixer(yaw, throttle):
        """
        Mix a pair of joystick axes, returning a pair of wheel speeds. This is where the mapping from
        joystick positions to wheel powers is defined, so any changes to how the robot drives should
        be made here, everything else is really just plumbing.
        
        :param yaw:
            Yaw axis value, ranges from -1.0 to 1.0
        :param throttle:
            Throttle axis value, ranges from -1.0 to 1.0
        :return:
            A pair of power_left, power_right integer values to send to the motor driver
        """

        clamp = lambda n, minn, maxn: max(min(maxn, n), minn)

        left = clamp(throttle + yaw, -1.0, 1.0)
        right = clamp(throttle - yaw, -1.0, 1.0)

        return (left * abs(throttle)), (right * abs(throttle))

    def buttons_pressed(self, data):
        print(data)
        if "start" in data:
            self.activate = not self.activate
            print("Autonomous activate?", self.activate)
        elif "l1" in data:
            print("Toggle headlights")
            self.headlights = not self.headlights
            self.queue.send("leds",
            {
                "zone": "front",
                "state": "on" if self.headlights else "off"
            })
        elif "circle" in data:
            self.queue.send("visual_debug", {})
        elif "square" in data:
            print("OPERATOR TRIGGERED ZONE VISITED")
            self.zone_visited = True
        else:
            print("Button not supported in this mode")

    def ball_seen(self, data):
        print("Seen {} ball of area {} at {}".format(data["colour"], data["area"], data["hoffset"]))

        if self.activate:
            self.ball_offset = data["hoffset"]
            self.mass_area = data["area"]
            self.ball_notified.set()

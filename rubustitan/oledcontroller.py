import os
from threading import Thread
from PIL import Image, ImageSequence, ImageFont
from time import sleep
from queue import Queue

from luma.core.sprite_system import framerate_regulator
from luma.core.render import canvas
from luma.core.virtual import viewport
try:
    from luma.oled.device import ssd1309
    from luma.core.interface.serial import spi
except:
    from luma.emulator.device import pygame

# from time import sleep

mode_strings = {
    "blastoff": "Blast Off",
    "marsmaze": "Canyons of Mars",
    "nebula": "Nebula Challenge",
    "spaceinvaders": "Space Invaders",
    "manualdrive": "Human control mode"
}

class OledController:
    def __init__(self, command_queue):
        self.queue = command_queue
        self.task_queue = Queue()
        self.thread = None
        self.running = False

    def start(self):
        self.queue.register_commands(
            {
                "mode_changed": self.mode_changed,
                "screen": self.screen_update
            })

        self.thread = Thread(target=self.thread_main)
        self.thread.start()

    def stop(self):
        print("Terminate OLED display")
        self.send("exit", {})

        self.queue.unregister_commands(["screen"])

        self.running = False
        self.thread.join()

    def scroll_message(self, message, font=None, speed=2):
        x = self.device.width

        # First measure the text size
        with canvas(self.device) as draw:
            w, h = draw.textsize(message, font)

        virtual = viewport(self.device, width=max(self.device.width, w + x + x), height=max(h, self.device.height))
        mid_h = (virtual.height / 2) - (h / 2)

        with canvas(virtual) as draw:
            draw.text((x, mid_h), message, font=font, fill="white")

        i = 0
        while i < x + w:
            virtual.set_position((i, 0))
            i += speed
            # sleep(0.025)

    def thread_main(self):
        try:
            interface = spi(device=0, port=0)
            self.device = ssd1309(interface)
        except:
            self.device = pygame()

        regulator = framerate_regulator(fps=15)

        if os.name == "nt":
            font_path = os.path.abspath(os.path.join(os.path.dirname(__file__), "fonts",
                        "bitwise.ttf"))
        else:
            font_path = "/usr/local/share/fonts/bitwise.ttf"

        # print("Loading font file '{}'".format(font_path))

        fnt = ImageFont.truetype(font_path, 40)

        running = True

        while running:
            # print("Cmd queue size = {}".format(self.task_queue.qsize()))
            item = self.task_queue.get()

            if item["action"] == "exit":
                print("Exit command received")
                running = False
            elif item["action"] == "message":
                self.scroll_message(item["obj"], font=fnt)
            elif item["action"] == "animation":
                data = item["obj"]

                img_path = os.path.abspath(os.path.join(os.path.dirname(__file__),
                    "animations", data["file"]))

                anim_gif = Image.open(img_path)

                if "loop" in data and data["loop"]:
                    while self.task_queue.empty():
                        for frame in ImageSequence.Iterator(anim_gif):
                            with regulator:
                                self.device.display(frame.convert(self.device.mode))

                            if not self.task_queue.empty():
                                break
                else:
                    for frame in ImageSequence.Iterator(anim_gif):
                        with regulator:
                            self.device.display(frame.convert(self.device.mode))

            else:
                print("Screen action not recognised:", item["action"])

            self.task_queue.task_done()

    def send(self, command, obj):
        item = {
            "action": command,
            "obj": obj
        }
        self.task_queue.put(item)

    def mode_changed(self, mode):
        print("New mode:", mode)
        self.send("message", mode_strings[mode])

        self.send("animation", {"file": "Idle.gif", "loop": True})

    def screen_update(self, data):
        self.send(data["action"], data["data"])

from gpiozero import CamJamKitRobot, DistanceSensor, LineSensor

class CamJamController:
    def __init__(self):
        self.robot = CamJamKitRobot()
        self.distance_sensor = DistanceSensor(18, 17)

    def move_left(self, speed):
        robot_speed = int(127 * speed)
        piconzero.spinLeft(robot_speed)

    def move_right(self, speed):
        robot_speed = int(127 * speed)
        piconzero.spinRight(robot_speed)

    def move_forwards(self, speed):
        robot_speed = int(127 * speed)
        piconzero.forward(robot_speed)

    def move_backwards(self, speed):
        robot_speed = int(127 * speed)
        piconzero.reverse(robot_speed)

    def move_stop(self):
        piconzero.stop();

    def read_distance(self):
    	return distance_sensor.distance()
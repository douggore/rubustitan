# Controller for bluedot

from bluedot import BlueDot
from time import sleep
from threading import Thread

class Controller():
    def __init__(self, command_queue):
        self.queue = command_queue

        self.bd = BlueDot()

        self.bd.when_pressed = self.bluedot_move
        self.bd.when_moved = self.bluedot_move
        self.bd.when_released = self.bluedot_release

    def start(self):
        pass

    def stop(self):
        pass

    def bluedot_move(self, position):
        print("X = {}, Y = {}".format(position.x, position.y))

        self.queue.send("operator_input",
            {
                "yaw": position.x,
                "throttle": position.y
            })

        #     new_action = False

        # # Get a ButtonPresses object containing everything that was pressed since the last
        # # time around this loop.
        # joystick.check_presses()
        
        # # Print out any buttons that were pressed, if we had any
        # if joystick.has_presses:
        #     print(joystick.presses)
        #     if "select" in joystick.presses:
        #         self.queue.send("cycle_mode", {})

        #     # If user presses the home button, trigger application shutdown
        #     if "home" in joystick.presses:
        #         self.queue.send("exit", {})

        #     self.queue.send("button_presses", list(joystick.presses))

    def bluedot_release(self, _):
        print("Blue stop")
        self.queue.send("operator_input",
            {
                "yaw": 0,
                "throttle": 0
            })

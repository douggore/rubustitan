from rubustitan import ColourProcessor, LineProcessor, EdgeProcessor, ProcessProxyQueue
from threading import Timer

from multiprocessing import Process, Pipe, Event, Queue

TARGET_FPS = 30
USE_MULTIPROCESSING = True

class VisionController:
    def __init__(self, queue, camera_interface):
        self.camera = None
        self.queue = queue
        self.shared_queue_local = Queue()
        self.shared_queue_remote = Queue()
        self.process_proxy_queue = None
        self.camera_interface = camera_interface
        # self.processor_class = None
        self.processor = None
        self.capturing = False
        self.frame_count = 0
        self.fps_count = 0
        self.dispatcher = None
        self.end_process = None
        self.pipe_in = None
        self.pipe_out = None

        self.queue.register_commands({
            "start_camera": self.start,
            "stop_camera": self.stop
        })

        self.processors = {
            # "ball": BallProcessor,
            # "armarker": ArMarkerProcessor
            "colour": ColourProcessor,
            "line": LineProcessor,
            "edge": EdgeProcessor
        }

    def cleanup(self):
        self.stop({})
        self.queue.unregister_commands(["start_camera", "stop_camera"])

    def run(self, pipe, processor_class, queue_local, queue_remote):
        self.pipe_out.close()
        proxy_queue = ProcessProxyQueue(queue_local, queue_remote)
        proxy_queue.start()

        processor = processor_class(proxy_queue)

        while not self.end_process.is_set():
            try:
                frame = pipe.recv()
                processor.process_image(frame)
            except EOFError:
                print("ERROR: Pipe no longer available")
        
        proxy_queue.stop()

    def start(self, data):
        capture_resolution = (640, 480)

        if data["processor"] in self.processors:
            if self.processor:
                del self.processor
                
            processor_class = self.processors[data["processor"]]
            capture_resolution = processor_class.PreferredResolution

        self.camera = self.camera_interface()
        self.camera.start_capture(self.image_callback, resolution=capture_resolution, framerate=TARGET_FPS)
        self.capturing = True

        if USE_MULTIPROCESSING:
            self.process_proxy_queue = ProcessProxyQueue(self.shared_queue_local, self.shared_queue_remote, self.queue)
            self.pipe_in, self.pipe_out = Pipe(duplex=False)
            self.process_proxy_queue.start()
            self.end_process = Event()
            self.dispatcher = Process(target=self.run, args=(self.pipe_in, processor_class, self.shared_queue_remote, self.shared_queue_local))
            self.dispatcher.start()
            self.pipe_in.close()
        else:
            self.processor = processor_class(self.queue)

        self.log_fps()

    def stop(self, data):
        if self.camera:
            self.camera.stop_capture()
        
        if self.capturing and USE_MULTIPROCESSING:
            print("Start closedown, with proxy queue")
            if self.process_proxy_queue:
                self.process_proxy_queue.stop()
            print("Close pipe")
            self.pipe_out.close()
            self.end_process.set()
            print("Finished closedown, perform process join")
            self.dispatcher.join()
            print("Finished join")

        self.capturing = False

    def image_callback(self, frame):
        self.frame_count += 1
        if USE_MULTIPROCESSING:
            self.pipe_out.send(frame)
        else:
            self.processor.process_image(frame)

    def log_fps(self):
        if (self.capturing):
            Timer(1.0, self.log_fps).start()

        if self.fps_count % 10 == 0:
            print("Real {} FPS, target {} FPS".format(self.frame_count, TARGET_FPS))

        self.frame_count = 0
        self.fps_count += 1
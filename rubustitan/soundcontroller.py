import pygame
import os
from time import sleep

class SoundController:
    def __init__(self, command_queue):
        self.queue = command_queue
        self.sounds_path = os.path.join(os.path.dirname(__file__), "sounds")

        pygame.mixer.pre_init(44100, -16, 2, 2048)
        # pygame.init()
        pygame.mixer.init()

    def start(self):
        self.queue.register_commands(
            {
                "mode_changed": self.mode_changed,
                # "button_presses": self.buttons_pressed,
                "get_sounds": self.get_sounds,
                "play_sound": self.play_sound_command
            })

    def stop(self):
        self.queue.unregister_commands(["mode_changed", "get_sounds", "play_sound"])

    def play_sound_command(self, data):
        if data["sound"].endswith(".wav"):
            self.play_sound(data["sound"])
        else:
            self.play_sound("{sound}.wav".format(sound=data["sound"]))
    
    def play_sound(self, filename, block = True):
        file_path = os.path.join(self.sounds_path, filename)

        try:
            sound_obj = pygame.mixer.Sound(file_path)
            sound_obj.play()

            if block:
                while pygame.mixer.get_busy() > 0:
                    sleep(1)
        except:
            print("Failed to play '{}'".format(filename))

    def mode_changed(self, mode):
        print("New mode:", mode)
        filename = "{mode}.wav".format(mode=mode)
        self.play_sound(filename, block=False)

    def buttons_pressed(self, buttons):
        if "dleft" in buttons:
            self.play_sound("iamrubustitan.wav")
        elif "dup" in buttons:
            self.play_sound("activate.wav")
        elif "dright" in buttons:
            self.play_sound("rainbow.wav")
        elif "ddown" in buttons:
            self.play_sound("straightline.wav")

    def get_sounds(self, data):
        sounds_list = os.listdir(self.sounds_path)
        self.queue.send("get_sounds_response", sounds_list)

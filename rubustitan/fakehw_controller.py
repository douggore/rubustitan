from datetime import datetime
from random import randint
from time import sleep
from threading import Thread

DISTANCES = [ 183, 204, 122, 136, 183, 272, 60 ]

class FakeHwController:
    def __init__(self, command_queue):
        self.queue = command_queue
        self.entry_time = datetime.now()
        self.running = False
        self.left_motor = 0.0
        self.right_motor = 0.0
        self.heading = 0
        self.accumulator = 0
        self.distance_iter = iter(DISTANCES)
        self.object_distance = next(self.distance_iter)

        self.queue.register_commands(
            {
                "hwcapabilities": self.hwcapabilities,
                "motor_speed": self.set_motor_speeds,
                "read_distance": self.read_distance,
                "read_heading": self.read_heading,
                "battery": self.read_battery,
                "projectile": self.launch_projectile
            })

        self.thread = Thread(target=self.thread_main)

        self.thread.start()

    def __del__(self):
        self.unregister()

    def unregister(self):
        self.running = False
        self.thread.join()
        self.queue.unregister_commands(["hwcapabilities", "motor_speed", "read_distance", "read_heading", "battery", "projectile"])

    def hwcapabilities(self):
        return {
            "motors": 2,
            "distance_sensors": 1,
            "distance_sensor_positions": ["front"],
            "absolute_heading": True
        }

    def set_motor_speed(self, motor, speed):
        # print("Set motor {} to speed {}".format(motor, speed))
        if motor == 0:
            self.left_motor = speed
        else:
            self.right_motor = speed

    def set_motor_speeds(self, data):
        for key, value in data.items():
            if key == "motor0":
                self.set_motor_speed(0, value)
            elif key == "motor1":
                self.set_motor_speed(1, value)

    def read_distance(self, data):
        # return distance_sensor.distance()
        self.queue.send("read_distance_response", {
                "front": self.object_distance #randint(20, 400)
            })

    def read_heading(self, data):
        self.queue.send("read_heading_response", {
            "heading": self.heading #randint(0, 359)
        })

    def read_battery(self, data):
        delta = datetime.now() - self.entry_time
        battery_percentage = 99 - (delta.seconds // 60)

        self.queue.send("battery_response", {
            "battery0": battery_percentage,
            "battery1": battery_percentage
        })

    def launch_projectile(self, data):
        sleep(5)
        self.queue.send("projectile_launched", {})

    def calc_new_angle(self, angle, delta):
        angle += delta

        if angle < 0.0:
            angle += 360.0
        elif angle > 360.0:
            angle -= 360.0

        return angle

    def thread_main(self):
        SLEEP_PERIOD = 0.1
        MAX_TURN_SPEED_PER_SEC = 90
        MAX_DISTANCE_PER_SEC = 80

        self.running = True

        while self.running:
            # Are the motors active
            if self.left_motor != 0 and self.right_motor != 0:
                # Are we spot turning?
                if self.left_motor + self.right_motor == 0:
                    delta = self.left_motor * (MAX_TURN_SPEED_PER_SEC * SLEEP_PERIOD)
                    self.heading = self.calc_new_angle(self.heading, delta)
                    self.accumulator += delta
                # Are we driving straight?
                elif self.left_motor == self.right_motor:
                    self.object_distance -= self.left_motor * (MAX_DISTANCE_PER_SEC * SLEEP_PERIOD)
                
                print("Acc = {:.1f}".format(self.accumulator))
                if self.accumulator >= 89.5:
                    self.accumulator -= 90
                    self.object_distance = next(self.distance_iter, 0)
                elif self.accumulator <= -89.5:
                    self.accumulator += 90
                    self.object_distance = next(self.distance_iter, 0)

            sleep(SLEEP_PERIOD)
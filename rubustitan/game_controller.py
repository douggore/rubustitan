# Controller for the game controller class

from approxeng.input.selectbinder import ControllerResource
from time import sleep
from threading import Thread

import approxeng.input

class Controller():
    def __init__(self, command_queue):
        self.thread = Thread(target=self.thread_main)
        self.queue = command_queue
        self.running = False

    def start(self):
        self.thread.start()

    def stop(self):
        print("Stop controller thread")
        self.running = False
        self.thread.join()
        print("Stopped controller thread")

    def thread_main(self):
        self.running = True

        while self.running:
            try:
                # Bind to any available joystick, this will use whatever's connected as long as the library
                # supports it.
                with ControllerResource(dead_zone=0.2, hot_zone=0.2) as joystick:
                    print("Controller found, press HOME button to exit, use left stick to drive.")
                    print(joystick.controls)
                    # Loop until the joystick disconnects, or we deliberately stop by raising a
                    # RobotStopException
                    last_x = None
                    last_lt = None
                    last_rt = None
                    cur_throttle = 0
                    new_action = False
                    while joystick.connected and self.running:
                        # Get joystick values from the left analogue stick
                        x_axis, left_trigger, right_trigger = joystick['lx', 'lt', 'rt']
                        
                        if left_trigger != last_lt:
                            cur_throttle = -left_trigger
                            new_action = True
                        elif right_trigger != last_rt:
                            cur_throttle = right_trigger
                            new_action = True
                        elif x_axis != last_x:
                            new_action = True

                        if new_action:
                            self.queue.send("operator_input",
                                {
                                    "yaw": x_axis,
                                    "throttle": cur_throttle
                                })

                            new_action = False

                        # Get a ButtonPresses object containing everything that was pressed since the last
                        # time around this loop.
                        joystick.check_presses()
                        
                        # Print out any buttons that were pressed, if we had any
                        if joystick.has_presses:
                            print(joystick.presses)
                            if "select" in joystick.presses:
                                self.queue.send("cycle_mode", {})

                            # If user presses the home button, trigger application shutdown
                            if "home" in joystick.presses:
                                self.queue.send("exit", {})

                            self.queue.send("button_presses", list(joystick.presses))

                        last_x = x_axis
                        last_lt = left_trigger
                        last_rt = right_trigger
                        
                        sleep(0.1)
            except IOError:
                # We get an IOError when using the ControllerResource if we don't have a controller yet,
                # so in this case we just wait a second and try again after printing a message.
                print("No controller found yet")
                sleep(5)

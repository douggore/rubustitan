# vision_test.py

# from rubustitan import VisionController, CommandQueue

from visioncontroller import VisionController
from cmdqueue import CommandQueue

try:
    from picamerainterface import PiCameraInterface as CameraInterface
except:
    from opencvcamerainterface import OpenCvCameraInterface as CameraInterface

import time

class Testing:
    def setup(self):
        print("Set ball colour")
        queue.send("set_ball_colour",
        {
            "colour": "red"
        })

        queue.register_commands({ "colour_detected": self.detected })

    def detected(self, data):
        print("Colour detected")

if __name__ == '__main__':
    testing = Testing()
    # queue = CommandQueue()
    with CommandQueue() as queue:
        vision_cont = VisionController(queue, CameraInterface)
        # vision_cont.start({ "processor": "armarker" })
        # vision_cont.start({ "processor": "ball" })
        # vision_cont.start({ "processor": "line" })
        # vision_cont.start({ "processor": "colour" })
        # vision_cont.start({ "processor": "edge" })

        queue.send("start_camera", { "processor": "colour" })

        time.sleep(2)

        testing.setup()

        try:
            time.sleep(50)
        finally:
            vision_cont.stop({})
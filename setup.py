from distutils.core import setup

setup(
    name='rubustitan',
    version='0.1.0',
    packages=['rubustitan',],
    include_package_data = True,
    license='MIT',
    long_description=open('README.md').read(),
    python_requires='>=3',
    entry_points={
        'console_scripts': [
            'rubustitan=rubustitan.main:main'
        ],
    }
)
